#ifndef GUI_H
#define GUI_H
#include "OtherAlgorithmIntent.h"
#include "ImageLoader.h"
#include "ImageProcessing.h"
// #include "multilayerperceptron.h"
// #include "myneural.h"
#include "mser.h"
#include "tensor_def.h"
#include "generation.h"
#include <stdio.h>
#include <gtk/gtk.h>
#include "dimension.h"
typedef struct UserInterface
{
    GtkWindow *window;
    GtkDrawingArea *area;
    GtkButton *start_button;
    GtkButton *rotate_button;
    GtkFileChooserButton *chooser;
    GtkCheckButton *grey_button;
    GtkAdjustment *tresh_scale;
    GtkCheckButton *mser_button;
    GtkAdjustment *min_scale;
    GtkAdjustment *max_scale;
    GtkAdjustment *k_scale;
    GtkCheckButton *mser_use;
    GtkCheckButton *stepping_tresh;
} UserInterface;

typedef struct MSER
{
    PixelDescriptor **mask;
    vector_MserRegion reg;
    vector_RegionDescriptor descriptors;
    bool exist;
} MSER;

typedef struct Knn
{
    vector_Tagged dataset;
    uint8_t k;
} Knn;

typedef struct
{
    vector_char index;
    vector_vector_vector_Neuron AI;
} NN;

NN loadNN(char *path)
{
    NN to_return;
    to_return.index = vector_of_char(0);
    to_return.AI = load_all_nns(&to_return.index, path);
    return to_return;
}

typedef struct OCR
{
    UserInterface ui;
    GreyScale_Image *grey;
    BMP_Image *source;
    MSER mser;
    Knn KnnInfo;
    vector_vector_Region legacy;
    NN NNInfo;
} OCR;

/**
 * @brief Return true if the new region is a new line.
 * 
 * @param old The old region
 * @param new The new region
 * @author tanguy.baltazart 
 * @return true If the new region is located on a new line.
 */
bool IsNewLine(Region old, Region new)
{

    return old.y + old.height < new.y;
}

bool IsSpace(Region old, Region cur, Region new)
{
    return cur.x - (old.x + old.width) > abs(new.x - (cur.x + cur.width)) + 5;
}

void strcat_c(char *str, char c)
{
    for (; *str; str++)
        ;
    *str++ = c;
    *str++ = 0;
}

char determine(OCR *info, GreyScale_Image *img)
{
    img = Resize(img, DIMENSION, DIMENSION);
#ifdef KNN
    uint8_t *point = point_from_image(img);
    char car = predict(point, info->KnnInfo.dataset, info->KnnInfo.k);
    free(point);
    return car;
#else
    char pos = identify(info->NNInfo.AI, img);
    return info->NNInfo.index.data[(size_t)pos];
#endif
}

void ProjectionGetChar(OCR *ocr, char *buff)
{
    Invert(ocr->grey);
    vector_Region regions = GetProjectionProfile(ocr->grey);

    for (size_t j = 0; j < regions.element_count; j++)
    {
        if (nth(regions, j).height == 0)
        {
            continue;
        }
        vector_Region column = Expand(ocr->grey, GetColumn(ocr->grey, &nth(regions, j)));
        for (size_t i = 0; i < column.element_count; i++)
        {

            GreyScale_Image *image = RegToSquare(column.data[i], ocr->grey);
            // char name[150];
            // sprintf(name, "/home/tbalta/ocr/src/test/image%lu.bmp", i);
            // BMP_SAVE(name, image);
            //        sprintf(buff,"%c",OCR_predict(image));
            //image = Resize(image, DIMENSION, DIMENSION);
            //uint8_t *point = point_from_image(image);
            //char car = predict(point, ocr->KnnInfo.dataset, ocr->KnnInfo.k);
            //char car = 'p';
            char car = determine(ocr, image);
            strcat_c(buff, car);
            if (i != 0 && IsNewLine(regions.data[i - 1], regions.data[i]))
            {
                strcat_c(buff, '\n');
            }
            else if (i >= 1 && i < regions.element_count &&
                     IsSpace(regions.data[i - 1],
                             regions.data[i],
                             regions.data[i + 1]))
            {
                strcat_c(buff, ' ');
            }
            strcat_c(buff, car);
        }
        delete_vec(column);
    }
    delete_vec(regions);
}

/**
 * @brief Detect and convert the character using the MSER algorithm
 * 
 * @param ocr Containing the OCR metadata
 * @param buff The buffer to put the character in.
 * @author tanguy.baltazart
 */
void MSER_getChar(OCR *ocr, char *buff)
{
    vector_MserRegion regions = ocr->mser.reg;
    PixelDescriptor **mask = ocr->mser.mask;

    for (size_t i = 0; i < regions.element_count; i++)
    {

        GreyScale_Image *image = MserToGreyScale(&regions.data[i], mask);
        //        sprintf(buff,"%c",OCR_predict(image));
        //image = Resize(image, DIMENSION, DIMENSION);
        //BMP_SAVE("myimage.bmp", image);
        //uint8_t *point = point_from_image(image);
        char car = determine(ocr, image);
        //predict(point, ocr->KnnInfo.dataset, ocr->KnnInfo.k);
        if (i != 0 && IsNewLine(regions.data[i - 1].location, regions.data[i].location))
        {
            strcat_c(buff, '\n');
        }
        else if (i >= 1 && i < regions.element_count &&
                 IsSpace(regions.data[i - 1].location,
                         regions.data[i].location,
                         regions.data[i + 1].location))
        {
            strcat_c(buff, ' ');
        }
        strcat_c(buff, car);
    }
}

/**
 * @brief Get the text on the image and put it in the buffer.
 * 
 * @param ocr Containing the metadata of the OCR
 * @param buff The buff to put the char in.
 */
void OCR_getChar(OCR *ocr, char *buff)
{
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_use)))
        MSER_getChar(ocr, buff);
    else
        ProjectionGetChar(ocr, buff);
}

#define delete2D(array, size)         \
    for (size_t i = 0; i < size; i++) \
        free((array[i]));             \
    free(array);

/**
 * @brief Execute MSER for the current image.
 * 
 * @param ocr Contain the metada of the ocr.
 * @author tanguy.baltazart
 */
void ExecuteMSER(OCR *ocr)
{
    // if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_use)))
    //{

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_button)))
    {

        if (ocr->mser.exist)
        {
            delete2D(ocr->mser.mask, ocr->source->number_of_rows);
            delete_vec(ocr->mser.descriptors);
            delete_vec(ocr->mser.reg);
        }
        uint32_t hight = (int)gtk_adjustment_get_value(ocr->ui.max_scale);
        uint32_t low = (int)gtk_adjustment_get_value(ocr->ui.min_scale);
        ocr->mser.reg = MserFindCharacter(ocr->grey, &(ocr->mser.mask), &(ocr->mser.descriptors), hight, low);
        ocr->mser.exist = true;
    }
    /*}
    else
    {
        if(ocr->legacy.allocated != 0){
            free_tensor2(ocr->legacy); 
        }
        vector_Region regions = GetProjectionProfile(ocr->grey);

        for (size_t j = 0; j < regions.element_count; j++)
        {
            if (nth(regions, j).height == 0)
            {
                continue;
            }
            vector_Region column = Expand(ocr->grey, GetColumn(ocr->grey, &nth(regions, j)));
            push_back_for_vector_Region(&(ocr->legacy), column);
        }
        delete_vec(regions);
    }*/
}

/**
 * @brief Load the file in the current ocr.
 * 
 * @param widget The button pressed
 * @param user_data The pointer to the OCR data structure.
 * @author tanguy.baltazart
 */
void load_file(GtkFileChooserButton *widget, gpointer user_data)
{
    OCR *ocr = user_data;
    gchar *file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(widget));
    if (ocr->mser.exist)
    {
        delete_vec(ocr->mser.reg);
        delete2D(ocr->mser.mask, ocr->source->number_of_rows);
        delete_vec(ocr->mser.descriptors);
        ocr->mser.exist = false;
    }

    /*
    if(ocr->source != NULL)
        FREEMAGE(ocr->source);
    if(ocr->grey != NULL)
        FREEMAGE(ocr->grey);
  */
    ocr->source = BMP_open(file);
    ocr->grey = ColorToGreyScale(ocr->source);
    ExecuteMSER(ocr);
    GtkDrawingArea *area = GTK_DRAWING_AREA(ocr->ui.area);
    gint x_max = gtk_widget_get_allocated_width(GTK_WIDGET(area));
    gint y_max = gtk_widget_get_allocated_height(GTK_WIDGET(area));
    gtk_widget_queue_draw_area(GTK_WIDGET(ocr->ui.area), 0, 0, x_max, y_max);
    printf("filename = %s \n", file);
}
#undef delete2D

/**
 * @brief Display a BMP image on the CR
 * 
 * @param cr pointer to Drawing area.
 * @param source Image to display
 * @author tanguy.baltazart 
 */
void PrintImageColor(cairo_t *cr, BMP_Image *source)
{

    for (size_t i = 0; i < source->number_of_rows; i++)
    {
        for (size_t j = 0; j < source->pixels_per_row; j++)
        {
            BMP_Pixel pixel = source->data[source->number_of_rows - i - 1][j];
            cairo_rectangle(cr, (double)j, (double)i, 1, 1);
            cairo_set_source_rgb(cr, (double)pixel.R / 255, (double)pixel.G / 255, (double)pixel.B / 255);
            cairo_fill(cr);
        }
    }
}

/**
 * @brief Draw the OCR in the drawing area.
 * 
 * @param cr Drawing area
 * @param user_data Pointer to the OCR data structure.
 * @author tanguy.baltazart
 */
gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
    // Gets the rectangle.
    OCR *ocr = user_data;

    cairo_set_source_rgb(cr, 51 / 255, 51 / 255, 51 / 255);
    cairo_paint(cr);
    gboolean displayGrey = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.grey_button));
    gboolean showSer = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_button));
    gboolean useMser = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_use));

    if ((!displayGrey || showSer) && ocr->source != NULL)
    {

        /*    if(!useMser && ocr->legacy.allocated != 0){
            BMP_Image *cop = copyColor(ocr->source);
            for(size_t i = 0; i < ocr->legacy.element_count; i++){
                DrawColorVectorRegion(cop,&(ocr->legacy.data[i]),255,0,0);
            }
            
            // drawMser2(cop,&(ocr->mser.descriptors),ocr->mser.mask);
            //DrawColorVectorMserRegion(cop,&(ocr->mser.reg),255,0,0);
            PrintImageColor(cr, cop);
            FREEMAGE(cop);

        }else */

        if (!useMser && !displayGrey && ocr->source != NULL)
        {

            BMP_Image *cop = copyColor(ocr->source);
            GreyScale_Image *greycop = copy(ocr->grey);
            Invert(greycop);
            ProjectionDetectChar(cop, greycop);
            PrintImageColor(cr, cop);
            FREEMAGE(cop);
            FREEMAGE(greycop);
        }
        else if (showSer && ocr->mser.exist)
        {
            BMP_Image *cop = copyColor(ocr->source);
            drawMser(cop, &(ocr->mser.reg), ocr->mser.mask);
            // drawMser2(cop,&(ocr->mser.descriptors),ocr->mser.mask);
            //DrawColorVectorMserRegion(cop,&(ocr->mser.reg),255,0,0);
            PrintImageColor(cr, cop);
            FREEMAGE(cop);
        }
        else
        {
            PrintImageColor(cr, ocr->source);
        }
    }
    else if (ocr->grey != NULL)
    {
        for (size_t i = 1; i < ocr->grey->number_of_rows; i++)
        {
            for (size_t j = 0; j < ocr->grey->pixels_per_row; j++)
            {

                GreyScale_Pixel pixel = ocr->grey->data[ocr->grey->number_of_rows - i][j];
                cairo_rectangle(cr, (double)j, (double)i, 1, 1);
                cairo_set_source_rgb(cr, (double)pixel / 255, (double)pixel / 255, (double)pixel / 255);
                cairo_fill(cr);
            }
        }
    }
    // Sets the background to white.
    // Propagates the signal.
    return FALSE;
}

VEC(vector_RegionDescriptor)

#define delete2D(array, size)         \
    for (size_t i = 0; i < size; i++) \
        free((array[i]));             \
    free(array);

size_t getMinIndex(vector_vector_RegionDescriptor *vec)
{
    size_t min = 0;

    for (size_t i = 0; i < vec->element_count; i++)
    {
        if (vec->data[i].element_count < vec->data[min].element_count)
            min = i;
    }

    return min;
}

/**
 * @brief Event to be triggred when the window close.
 * 
 * @author tanguy.baltazart 
 */
void on_destroy(GtkWidget *widget, gpointer user_data)
{
    OCR *ocr = user_data;

    if (ocr->mser.mask != NULL)
    {
        delete2D(ocr->mser.mask, ocr->source->number_of_rows);
    }

    if (ocr->grey != NULL)
    {
        FREEMAGE(ocr->grey);
    }

    if (ocr->source != NULL)
    {
        FREEMAGE(ocr->source);
    }

    delete_vec(ocr->mser.descriptors);
    delete_vec(ocr->mser.reg);

    gtk_main_quit();
}

/**
 * @brief Redraw the drawing area
 * 
 * @param area THe area to redraw.
 * @author tanguy.baltazart
 */
void redraw(GtkDrawingArea *area)
{
    gint x_max = gtk_widget_get_allocated_width(GTK_WIDGET(area));
    gint y_max = gtk_widget_get_allocated_height(GTK_WIDGET(area));
    gtk_widget_queue_draw_area(GTK_WIDGET(area), 0, 0, x_max, y_max);
}

/**
 * @brief Redraw the area
 * 
 * @author tanguy.baltazart 
 */
void toggle_redraw(GtkToggleButton *toggle, gpointer user_data)
{
    OCR *ocr = user_data;
    redraw(ocr->ui.area);
}

/**
 * @brief Update the treshhold for the ocr.
 * 
 * @author tanguy.baltazart
 */
void update_tresh(GtkAdjustment *adjustement, gpointer user_data)
{
    OCR *ocr = user_data;
    if (ocr->source == NULL)
        return;
    GreyScale_Image *copy = ColorToGreyScale(ocr->source);
    if (gtk_toggle_button_get_active(ocr->ui.stepping_tresh))
    {
        ApplyTreshHold(copy, (int)gtk_adjustment_get_value(adjustement));
    }
    else
    {
        SteppingTreshHold(copy, ((int)gtk_adjustment_get_value(adjustement)));
    }
    if (ocr->grey != NULL)
    {
        // FREEMAGE(ocr->grey);
        for (uint32_t i = 0; i < ocr->grey->number_of_rows; i++)
            free(ocr->grey->data[i]);
        free(ocr->grey->data);
        free(ocr->grey);
    }
    ocr->grey = copy;
    // if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_button)))
    ExecuteMSER(ocr);

    redraw(ocr->ui.area);
}

/**
 * @brief Update the display when a button is pressed.
 * @author tanguy.baltazart
 * 
 * @param togglebutton 
 * @param user_data 
 */
void update_display(GtkToggleButton *togglebutton, gpointer user_data)
{

    OCR *ocr = user_data;
    // if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_button)))
    ExecuteMSER(ocr);
    redraw(GTK_DRAWING_AREA(ocr->ui.area));
}

/**
 * @brief Start the OCR and save the text in file.
 * @author tanguy.baltazart
 * 
 * @param user_data 
 */
void start_OCR(GtkButton *button, gpointer user_data)
{
    OCR *ocr = user_data;
    if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ocr->ui.mser_button)))
        ExecuteMSER(ocr);

    char buff[4728] = "";

    OCR_getChar(ocr, buff);
    write_to(buff, "OCR_output", pdf);
    write_to(buff, "OCR_output", html);
    write_to(buff, "OCR_output", txt);
    write_to(buff, "OCR_output", odt);
    printf("text: %s", buff);
}

/**
 * @brief Free the mser data allocated in GTK
 * @author tanguy.baltazart
 * 
 * @param ocr 
 */
void FREEMSER(OCR *ocr)
{

    if (ocr->mser.exist)
    {
        delete2D(ocr->mser.mask, ocr->source->number_of_rows);
        delete_vec(ocr->mser.descriptors);
        delete_vec(ocr->mser.reg);
        ocr->mser.exist = false;
    }
}

/**
 * @brief Start the rotation of the image
 * @author tanguy.baltazart
 * 
 * @param button 
 * @param user_data 
 */
void rotate(GtkButton *button, gpointer user_data)
{
    OCR *ocr = user_data;
    // printf("plouf");
    GreyScale_Image *temp = ColorToGreyScale(ocr->source);
    GreyScale_Image *sobel = Sobel(temp);
    double angle = GetAngle(sobel);
    GreyScale_Image *rotated = RotatebyAngle(temp, angle);
    FREEMAGE(sobel);
    FREEMAGE(temp);
    // Clean_image1(rotated);
    Clean_image2(rotated);
    // FREEMAGE(ocr->grey);
    FREEMSER(ocr);
    BMP_SAVE("temp.bmp", rotated);
    FREEMAGE(ocr->grey);
    FREEMAGE(ocr->source);
    ocr->source = BMP_open("temp.bmp");

    ocr->grey = ColorToGreyScale(ocr->source);
    // FREEMAGE(rotated);

    // FREEMAGE(grey);
    // FREEMAGE(source);

    ExecuteMSER(ocr);
    // printf("rotation");
    redraw(ocr->ui.area);
}

/**
 * @brief Update mser on the display
 * @author tanguy.baltazart
 * 
 * @param adjustement 
 * @param user_data 
 */
void updateMser(GtkAdjustment *adjustement, gpointer user_data)
{
    OCR *ocr = user_data;
    ExecuteMSER(ocr);
    redraw(ocr->ui.area);
}

void update_knn(GtkAdjustment *adjustement, gpointer user_data)
{
    OCR *ocr = user_data;    
    ocr->KnnInfo.k = (int)gtk_adjustment_get_value(adjustement);
}

/**
 * @brief Init the gui
 * @author tanguy.baltazart daniel.frederic
 * @return int 
 */
int startGui()
{

    gtk_init(NULL, NULL);

    // Constructs a GtkBuilder instance.
    GtkBuilder *builder = gtk_builder_new();

    // Loads the UI description.
    // (Exits if an error occurs.)
    GError *error = NULL;
    if (gtk_builder_add_from_file(builder, "ocr.glade", &error) == 0)
    {
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }

    GtkWindow *window = GTK_WINDOW(gtk_builder_get_object(builder, "org.gtk.ocr"));
    GtkDrawingArea *area = GTK_DRAWING_AREA(gtk_builder_get_object(builder, "area"));
    GtkFileChooserButton *chooser = GTK_FILE_CHOOSER_BUTTON(gtk_builder_get_object(builder, "chooser"));
    GtkButton *start_button = GTK_BUTTON(gtk_builder_get_object(builder, "start_button"));
    GtkButton *rotate_button = GTK_BUTTON(gtk_builder_get_object(builder, "rotate_button"));
    GtkCheckButton *grey_button = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "grey_button"));
    GtkAdjustment *tresh_scale = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment1"));
    GtkAdjustment *min_scale = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment2"));
    GtkAdjustment *max_scale = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment3"));
    GtkAdjustment *k_scale = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adjustment4"));
    GtkCheckButton *mser_button = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "mser_button"));
    GtkCheckButton *stepping_tresh = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "stepping_tresh"));
    GtkCheckButton *mser_use = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "mser_use"));

    OCR ocr = (OCR){
        .ui = (UserInterface){
            .window = window,
            .area = area,
            .chooser = chooser,
            .start_button = start_button,
            .grey_button = grey_button,
            .tresh_scale = tresh_scale,
            .mser_button = mser_button,
            .max_scale = max_scale,
            .min_scale = min_scale,
            .stepping_tresh = stepping_tresh,
            .mser_use = mser_use,
            .k_scale = k_scale,
            .rotate_button = rotate_button},
        .grey = NULL,
        .source = NULL,
        .mser = (MSER){.mask = NULL, .reg = vector_of_MserRegion(0), .descriptors = vector_of_RegionDescriptor(0)},
        .KnnInfo = (Knn){.dataset = dataset_to_tagged("./encore_un_dataset.bmp/dataset", 8, 33, 126),
                         //  .dataset = NULL,
                         .k = VOTE},

    };
#ifndef KNN
    ocr.NNInfo = loadNN("../poids");
#else
    ocr.KnnInfo = (Knn){
        .dataset = dataset_to_tagged("./dataset_knn/dataset", 8, 33, 126),
        .k = VOTE};
#endif

    // vector_Tagged dataset = dataset_to_tagged("/home/daniel/datasets/ocr/script1/", 10, 33, 126);
    g_signal_connect(window, "destroy", G_CALLBACK(on_destroy), &ocr);
    g_signal_connect(chooser, "file-set", G_CALLBACK(load_file), &ocr);
    g_signal_connect(area, "draw", G_CALLBACK(on_draw), &ocr);
    g_signal_connect(GTK_CHECK_BUTTON(grey_button), "toggled", G_CALLBACK(toggle_redraw), &ocr);
    g_signal_connect(tresh_scale, "value-changed", G_CALLBACK(update_tresh), &ocr);
    g_signal_connect(max_scale, "value-changed", G_CALLBACK(updateMser), &ocr);
    g_signal_connect(min_scale, "value-changed", G_CALLBACK(updateMser), &ocr);
    g_signal_connect(k_scale, "value-changed", G_CALLBACK(update_knn), &ocr);
    g_signal_connect(mser_button, "toggled", G_CALLBACK(update_display), &ocr);
    g_signal_connect(mser_use, "toggled", G_CALLBACK(update_display), &ocr);
    g_signal_connect(start_button, "clicked", G_CALLBACK(start_OCR), &ocr);
    g_signal_connect(rotate_button, "clicked", G_CALLBACK(rotate), &ocr);
    gtk_main();

    return 0;
}
#endif