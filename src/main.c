#include "ImageLoader.h"
#include "ImageProcessing.h"
// #include "multilayerperceptron.h"
// #include "myneural.h"
#include "mser.h"
#include "tensor_def.h"
#include <stdio.h>
#include "gui.h"

#define delete2D(array, size)                                                  \
    for (size_t i = 0; i < size; i++)                                          \
        free((array[i]));                                                      \
    free(array);



int main() {

    startGui();
    /*
    BMP_Image* source = BMP_open("./src/test1.bmp");
    // BMP_Image* source = BMP_open("./src/txt_droit.bmp");
    GreyScale_Image* grey = ColorToGreyScale(source);
    Invert(grey);
    ApplyTreshHold(grey, 90);
    PixelDescriptor** mask;
    vector_RegionDescriptor descriptors;
    vector_MserRegion reg = MserFindCharacter(grey, &mask, &descriptors, 20);


    for (size_t i = 0; i < reg.element_count; i++) {
        char buff[DIMENSION];
        // GreyScale_Image* result = MserToGreyScale(&reg.data[i], mask);
         GreyScale_Image* result = ExtractRegion(grey, &reg.data[i].location);
        sprintf(buff, "./test/%04zui.bmp", i);
        BMP_SAVE(buff, result);
        FREEMAGE(result);
    }
    drawMser2(source, &descriptors,mask);
    DrawColorVectorMserRegion(source, &reg, 255, 0, 0);
    SAVE_IMAGE("mser.bmp", source);
    BMP_SAVE("msergrey.bmp", grey);

    delete2D(mask, source->number_of_rows);
    delete_vec(descriptors);
    delete_vec(reg);
    FREEMAGE(source);
    FREEMAGE(grey);
 */
   return 0;
}