#pragma once
#include "ImageProcessing.h"
#include "tensor_def.h"
#include "myneural.h"
#include <stdio.h>

#ifdef RELEASE
#define printf(x, ...) 
#endif
/*
vector_float compute_error_in_layer(vector_vector_Neuron* network, vector_float result,
    vector_float expected, uint64_t cur_layer, vector_float errors){
    size_t i = (size_t)cur_layer;
    vector_Neuron* layer = &network->data[i];

    if (i != network->element_count - 1) {

        for (size_t j = 0; j < layer->element_count; j++) {
            float error = 0.0;

            for (size_t z = 0; z < network->data[i + 1].element_count;
                    z++) {

                error += network->data[i + 1].data[z].weights.data[j] *
                            network->data[i + 1].data[z].delta;
            }

            push_back_for_float(&errors, error);
        }

    } else {
        for (size_t j = 0; j < layer->element_count; j++) {
            Neuron neuron = layer->data[j];
            push_back_for_float(&errors, nth(expected, j) - neuron.output);
        }
    }
    return errors;
}

vector_float compute_error_in_network(vector_vector_Neuron* network, vector_float result,
    vector_float expected) {
    for (int64_t C = network->element_count - 1; C != -1; C--) {

    }
}*/
/**
 * @brief Backward propagation
 * @author daniel.frederic
 * @param network The neural network
 * @param expected The expected value
 */
void backward_propagate_batch(vector_vector_Neuron* network, vector_float expected) {

    for (int64_t C = network->element_count - 1; C != -1; C--) {
        size_t i = (size_t)C;
        vector_float errors = vector_of_float(0);
        vector_Neuron layer = network->data[i];

        if (i != network->element_count - 1) {

            for (size_t j = 0; j < layer.element_count; j++) {
                float error = 0.0;

                for (size_t z = 0; z < network->data[i + 1].element_count;
                     z++) {

                    error += network->data[i + 1].data[z].weights.data[j] *
                             network->data[i + 1].data[z].delta;
                }

                push_back_for_float(&errors, error);
            }

        } else {
            for (size_t j = 0; j < layer.element_count; j++) {
                Neuron neuron = layer.data[j];
                push_back_for_float(&errors, nth(expected, j) - neuron.output);
            }
        }

        for (size_t j = 0; j < network->data[i].element_count; j++) {
            // printf("%d %d %d %d\n", i, j, errors.element_count, network->data[i].data[j].deltas_batch.element_count);
            push_back_for_float(&(network->data[i].data[j].deltas_batch), 
                nth(errors, j) * dsigmoid(network->data[i].data[j].output));
            /*layer->data[j].deltas_batch.data[j] =  
                nth(errors, j) * dsigmoid(layer->data[j].output);*/
        }
        delete_vec(errors);
    }
}


void mean_error(vector_vector_Neuron* nn){
    for(int i = 0; i < nn->element_count; i++)
        for(int y = 0; y < nth(*nn, i).element_count; y++){
            float mean = 0;
            for(int z = 0; z < nth2(*nn, i, y).deltas_batch.element_count; z++){
                // // printf("%d %d %d\n", i, y, z);
                mean += nth(nth2(*nn, i, y).deltas_batch, z);
            }
            //mean /= nth2(*nn, i, y).deltas_batch.element_count;
            nth2(*nn, i, y).delta = mean;
            delete_vec(nth2(*nn, i, y).deltas_batch);
            nth2(*nn, i, y).deltas_batch = vector_of_float(100);
        }
}

/**
 * @brief Trains a neural network
 * 
 * @param nn Pointer to neural network
 * @param data Data for training
 * @param expected Expected values for last layers
 * @param n_epoch epoch
 */
void train_batch(vector_vector_Neuron* nn, vector_vector_float data,
                   vector_vector_float expected, size_t n_epoch) {
    for (size_t j = 0; j < n_epoch; j++) {
        // printf("\t\t%d\n", j+1);
        for (size_t i = 0; i < data.element_count; i++) {
            vector_float onichan = forward_propagate(nn, nth(data, i));
            delete_vec(onichan);
            backward_propagate_batch(nn, nth(expected, i));
        }
        mean_error(nn);
        for (size_t i = 0; i < data.element_count; i++) {
            update_weights(nn, nth(data, i), 0.5);
        }
    }
}

vector_float image_to_layer(GreyScale_Image image) {
    vector_float layer = vector_of_float(image.number_of_pixels);
    for(int i = 0; i < image.number_of_rows; i++)
        for(int y = 0; y < image.pixels_per_row; y++)
            push_back_for_float(&layer, (float)(image.data[i][y]));
    return layer;
}

/*
        uen ligne par neurone

        une ligne avec / ça fait changer de couche
*/
void save_nn(vector_vector_Neuron nn, char id){
    char filename[100]; 
    sprintf(filename, "weights/%03d.weights", (int)id);
    FILE* in = fopen(filename, "w");
    for(size_t i = 0; i < nn.element_count; i++){
        for(size_t y = 0; y < nth(nn, i).element_count; y++){
            for(size_t z = 0; z < nth2(nn, i, y).weights.element_count; z++)
                fprintf(in, "%f ", nth2(nn, i, y).weights.data[z]);
            fprintf(in, "\n");
        }
        fprintf(in, "/\n");
    }
}

vector_vector_Neuron load_nn(char id, char* path){
    vector_vector_Neuron nn = vector_of_vector_Neuron(0);
    push_back_for_vector_Neuron(&nn, vector_of_Neuron(0));
    char filename[100];
    sprintf(filename, "%s/%d", path, (int)id);
    FILE* in = fopen(filename, "r");
    char * line = NULL;
    size_t len = 0;
    size_t read;
    if (!in)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, in)) != -1) {
        if(read < 5){
            push_back_for_vector_Neuron(&nn, vector_of_Neuron(0));
        } else{
            push_back_for_Neuron(
                &(nn.
                    data[nn.element_count-1]), 
                (Neuron){
                    .weights = vector_of_float(100),
                    .delta = 0,
                    .output = 0});
            float f;
            char* token = strtok(line, " ");
            while( token != NULL ) {
                sscanf(token, "%f", &f);
                push_back_for_float(
                    &(nn.
                        data[nn.element_count-1].
                        data[nn.data[nn.element_count-1].element_count-1].weights),
                    f);
                token = strtok(NULL, " ");
            }

            nn.data[nn.element_count-1].
                data[nn.data[nn.element_count-1].element_count-1].
                    weights.element_count--;

            /*while(sscanf(line, "%f", &f)+1)
                push_back_for_float(
                    &(nn.
                        data[nn.element_count-1].
                        data[nn.data[nn.element_count-1].element_count-1].weights),
                    f);*/
        }
        //// printf("Retrieved line of length %zu:\n", read);
        //// printf("%s", line);
    }

    fclose(in);
    if (line)
        free(line);

    Remove_at_for_vector_Neuron(&nn, nn.element_count-1);
    
    return nn;
}

VEC(vector_vector_Neuron)
VEC(char)

vector_vector_vector_Neuron load_all_nns(vector_char* index, char* path){
    vector_vector_vector_Neuron AI = vector_of_vector_vector_Neuron(100);
    char buff[255];
    sprintf(buff, "%s/list.txt", path);
    FILE* in = fopen(buff, "r");
    char * line = NULL;
    size_t len = 0;
    size_t read;
    int id;
    while ((read = getline(&line, &len, in)) != -1) {
        if(read < 5)
            break;
        fscanf(in, "%d", &id);
        push_back_for_vector_vector_Neuron(&AI, load_nn(path, (char)id));
        push_back_for_char(index, (char)id);
    }
    return AI;
}

char identify(vector_vector_vector_Neuron AI, GreyScale_Image* img){
    //Resize(img, DIMENSION, DIMENSION);
    //Resize(img, DIMENSION-28, 40-3);
    //vector_float layer = vector_of_float(DIMENSION*DIMENSION);
    float max = 0;
    char max_pos = 0;
    for(int i = 0; i < AI.element_count; max_pos++, i++){
        vector_float l = image_to_layer(*img);
        float f = forward_propagate(&(AI.data[i]), l).data[0];
        //printf("probability for %c : %f%%.\n", (char)i+33, f*100);
        if(f > max && f < 1.){
            max = f;
            max_pos = i;
        }
    }
    return max_pos + 33;
}

#define GEN 1
void train_dataset(const char* path) {
    char dir[75];
    vector_vector_Neuron nn = init_network(DIMENSION*DIMENSION, 40, 1, 8);
    vector_vector_float expected = vector_of_vector_float(128-33);
    for(char c = 33; c < 127; c++){
        push_back_for_vector_float(&expected, vector_of_float(8));
        for(int64_t i = 0; i < 8; i++)
            push_back_for_float(&(nth(expected, c-33)), (float)((c >> i) & 0x01));
    }
    for(int i = 0; i <= GEN; i++ ){
        // printf("%d\n", i);
        vector_vector_float batch = vector_of_vector_float(128-33);
        for(int c = 65; c < 127; c++){
            //// printf("\t%d\n", c);
            sprintf(dir, "%s/%03d/%d.bmp", path, c, i);
            BMP_Image* img = BMP_open(dir);
            GreyScale_Image* grey = ColorToGreyScale(img);
            ApplyTreshHoldbin(grey, 140);
            push_back_for_vector_float(&batch, image_to_layer(*grey));
            FREEMAGE(img);
            FREEMAGE(grey);
        }
        train_batch(&nn, batch, expected, 10);;
    }
    //save_nn(nn);


}
#undef GEN

#undef printf

/*void train_network(vector_vector_Neuron* nn, vector_vector_vector_float data,
                   vector_vector_float expected, size_t n_epoch) {
    vector_vector_float batch = vector_of_vector_float(0);
    for(size_t i = 0; i < data.element_count; i++){

    }
}*/
