#pragma once
#include "ImageProcessing.h"
#include "tensor_def.h"
#include "mser.h"
#include "dimension.h"
#include "assert.h"
#include "training.h"
#define IMAGE_DIMENSION (DIMENSION*DIMENSION)
#define IMAGE_WIDTH DIMENSION
#define Pointelement uint8_t
#define Point Pointelement*
#define LPoint(x) Point x = (Point)calloc(IMAGE_DIMENSION, sizeof(Pointelement))

VEC(Pointelement)
VEC(Point)

#define binarize(x) ((x) < 100 ? 0 : EJECTION)
Point point_from_image(GreyScale_Image* img){
    assert(img->pixels_per_row = DIMENSION);
    assert(img->number_of_rows = DIMENSION);
    LPoint(r);
    for(size_t i = 0; i < img->number_of_rows; i++){
        for(size_t y = 0; y < img->pixels_per_row; y++){
            r[IMAGE_WIDTH*i+y] = binarize(img->data[i][y]);
        }
    }
    return r;
}
#undef binarize

float distance(Point a, Point b){
    float sum = 0;
    for (int i = 0; i < IMAGE_DIMENSION; i++)
        sum = sum + pow(a[i] - b[i], 2);
    return sqrt(sum);
}

typedef struct {
    Point p;
    char cat;
} Tagged;

typedef struct {
    Tagged d;
    float dist;
} Pair;


VEC(Pair);
VEC(uint8_t)
VEC(Tagged);

void swap(Pair *xp, Pair *yp) { 
    Pair temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
} 
  
void bubbleSort(vector_Pair arr) { 
    size_t i, j; 
    for (i = 0; i < arr.element_count-1; i++)  
        for (j = 0; j < arr.element_count-i-1; j++)  
            if (arr.data[j].dist > arr.data[j+1].dist) 
                swap(&arr.data[j], &arr.data[j+1]); 
} 


vector_Tagged searchnn(vector_Tagged entrainement, Point donnee_test, 
        size_t nbVoisins){
    // vector_Pair distances = vector_of_Pair(300);
    vector_Pair distances = vector_of_Pair(entrainement.element_count+1);
    for(size_t i = 0; i < entrainement.element_count; i++){
        float dist = distance(donnee_test, entrainement.data[i].p);
        push_back_for_Pair(&distances, (Pair){entrainement.data[i], dist});
    }
    bubbleSort(distances);
    vector_Tagged kvoisins = vector_of_Tagged(nbVoisins);
    for(size_t i = 0; i < nbVoisins; i++)
        push_back_for_Tagged(&kvoisins, nth(distances, i).d);
    delete_vec(distances);
    return kvoisins;
}

typedef struct {
    char c;
    uint8_t repr;
} histo_element;

VEC(histo_element)

uint8_t predict(Point donnee_test, vector_Tagged entrainement, 
        size_t nbVoisins){
    vector_Tagged voisins = searchnn(entrainement, donnee_test, nbVoisins);
    vector_char sortie = vector_of_char(entrainement.element_count);
    for(size_t i = 0; i < voisins.element_count; i++){
        push_back_for_char(&sortie, voisins.data[i].cat);
    }
    vector_histo_element histo = vector_of_histo_element(sortie.element_count);
    for(size_t i = 0; i < sortie.element_count; i++){
        char c = sortie.data[i];
        int found = 0;
        for(size_t y = 0; y < sortie.element_count; y++){
            if(histo.data[y].c == c){
                found = 1;
                histo.data[y].repr++;
            }
        }
        if(!found){
            push_back_for_histo_element(&histo, (histo_element){.c = c, .repr = 1});
        }
    }
    uint8_t max = 0;
    char max_v = histo.data[0].c;
    for(size_t i = 0; i < histo.element_count; i++){
        if(histo.data[i].repr > max){
            max = histo.data[i].repr;
            max_v = histo.data[i].c;
        }
    }
    
    return max_v;
}

#undef delete2D
#define delete2D(array, size) for(size_t i = 0; i < size;i++) free((array[i])); free(array);
vector_Tagged dataset_to_tagged(const char* path, int max_index, int begin, int end){
    char buffer[150];
    vector_Tagged r = vector_of_Tagged(150);
    for(int i = begin; i <= end; i++){
        sprintf(buffer, "%s/%03d/n", path, i);
        FILE* in = fopen(buffer, "r");
        int fnnum = 1;
        if(in){
            fscanf(in, "%d", &fnnum);
            fclose(in);
        }
        fnnum = 1;
        for(int y = 0; y < max_index * fnnum; y++){
            sprintf(buffer, "%s/%03d/%d.bmp", path, i, y);
            BMP_Image* col = BMP_open(buffer);
            if(!col)
                continue;
            SAVE_IMAGE("temp3.bmp", col);
            if(!col){
                printf("Unable to open '%s'\nAborting.", buffer);
                exit(1);
            }
            GreyScale_Image* grey = ColorToGreyScale(col);
            //Invert(grey);
            ApplyTreshHold(grey, 125);
            BMP_SAVE("temp2.bmp", grey);
            grey = Resize(grey, IMAGE_WIDTH, IMAGE_DIMENSION/IMAGE_WIDTH);
            BMP_SAVE("temp.bmp", grey);
            /*Invert(img);
            ApplyTreshHold(img, 90);
            PixelDescriptor** mask;
            vector_RegionDescriptor regdes;
            vector_MserRegion v =  MserFindCharacter(img, &mask, &regdes, img->number_of_pixels/10, 0);
            //FREEMAGE(img);
            img = Resize(MserToGreyScale(&(v.data[0]), mask), DIMENSION, DIMENSION);*/

            PixelDescriptor** mask;
            vector_RegionDescriptor descriptors;
            vector_MserRegion reg = MserFindCharacter(grey, &mask, &descriptors, 1, 20);

            //RotatebyAngle(img, GetAngle(img));
            push_back_for_Tagged(&r, (Tagged){.p = point_from_image(grey), .cat = (char)i});
            delete_vec(descriptors);
            delete_vec(reg);
            delete2D(mask, grey->number_of_rows);
        }
    }
    return r;
}
#undef delete2D
