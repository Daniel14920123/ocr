/**
 * @file ImageProcessing.h
 * @author daniel.frederic nicolas.kourban tanguy.baltazart
 * @brief All of the image processing is here
 * @version 1.2
 * @date 2020-10-25
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef IMAGEPROCESSING_HEADER_INCLUDED
#define IMAGEPROCESSING_HEADER_INCLUDED
#include "dimension.h"
#include "ImageLoader.h"
#include "tensor_def.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
/**
 * @brief Pi
 *
 */
#define PI 3.14159265

#define FREEMAGE(image)                                                        \
    for (uint32_t i = 0; i < image->number_of_rows; i++)                       \
        free(image->data[i]);                                                  \
    free(image->data);                                                         \
    free(image);

/*
    Here we should write functions such as contrast, B&W, high pass filter,
    and the splitting of files and symbols
*/
/*
    SECTION GREYSCALE
*/

/**
 * @brief A Pixel is stored on one byte, the luminance
 * @author daniel.frederic
 *
 */
typedef uint8_t GreyScale_Pixel;

/**
 * @brief A Greyscale image, following the same structure as a BMP one but
 * storing luminances
 * @author daniel.frederic
 */
typedef struct {
    GreyScale_Pixel** data; // y puis x
    uint32_t pixels_per_row;
    uint32_t number_of_rows;
    uint32_t number_of_pixels;
} GreyScale_Image;

/**
 * @brief A Piece of image
 * @author tanguy.baltazart
 */
typedef struct {
    uint32_t x;
    uint32_t y;
    uint32_t width;
    uint32_t height;
} Region;

/**
 * @brief Metaprogrammates a vector type for Regions.
 * @author tanguy.baltazart
 */
VEC(Region)
VEC(vector_Region);

/**
 * @brief Chained list for Region
 * @authors tanguy.baltazard daniel.frederic
 */
typedef struct Vector_region {
    Region region;
    struct Vector_region* next;
} Vector_region;

/**
 * @brief Transforms a Color Pixel to a Greyscale Pixel
 * @author daniel.frederic
 * @param input The pixel to transformate
 * @return GreyScale_Pixel the returned pixel
 */
GreyScale_Pixel ColorToGreyScale_p(BMP_Pixel input) {
    return (GreyScale_Pixel)(0.2126 * (float)input.R + 0.7152 * (float)input.G +
                             0.0722 * (float)input.B);
}

/**
 * @brief Transforms a Colored image to a Greyscaled Image
 * @author daniel.frederic
 * @param input Pointer to a BMP_Image, the image to transform
 * @return GreyScale_Image* Pointer to the transformed image
 */
GreyScale_Image* ColorToGreyScale(const BMP_Image* input) {
    GreyScale_Image* to_return =
        (GreyScale_Image*)malloc(sizeof(GreyScale_Image));
    to_return->number_of_rows = input->number_of_rows;
    to_return->pixels_per_row = input->pixels_per_row;
    to_return->number_of_pixels = input->pixels_per_row * input->number_of_rows;
    to_return->data = (GreyScale_Pixel**)malloc(input->number_of_rows *
                                                sizeof(GreyScale_Pixel*));
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        to_return->data[i] = (GreyScale_Pixel*)malloc(input->pixels_per_row *
                                                      sizeof(GreyScale_Pixel));
    }

    for (uint32_t i = 0; i < (uint32_t)(to_return)->number_of_rows; i++) {
        for (uint32_t j = 0; j < (uint32_t)(to_return)->pixels_per_row; j++) {
            to_return->data[i][j] = ColorToGreyScale_p(input->data[i][j]);
        }
    }
    return to_return;
}
/**
 * @brief Create an image with defined size
 *
 * @author tanguy.baltazart
 * @param width width of the image
 * @param height height of the image
 * @param default_value default value for the pixel
 * @return GreyScale_Image*
 */
GreyScale_Image* Create_Image(int32_t width, int32_t height,
                              int default_value) {
    GreyScale_Image* to_return =
        (GreyScale_Image*)calloc(1, sizeof(GreyScale_Image));
    to_return->number_of_rows = height;
    to_return->pixels_per_row = width;
    to_return->number_of_pixels = width * height;
    to_return->data =
        (GreyScale_Pixel**)calloc(height, sizeof(GreyScale_Pixel*));
    for (int i = 0; i < height; i++) {
        to_return->data[i] =
            (GreyScale_Pixel*)calloc(width, sizeof(GreyScale_Pixel));
    }

    for (uint32_t i = 0; i < (uint32_t)(to_return)->number_of_rows; i++) {
        for (uint32_t j = 0; j < (uint32_t)(to_return)->pixels_per_row; j++) {
            to_return->data[i][j] = default_value;
        }
    }
    return to_return;
}

/**
 * @brief Extract a 3 * 3 matrix centered at x y
 *
 * @author tanguy.baltazart
 * @param input The image to extract from
 * @param x x coordinate fo the pixel
 * @param y y coordinate of the pixel
 * @return GreyScale_Pixel**
 */
GreyScale_Pixel** Extract(const GreyScale_Image* input, uint32_t x,
                          uint32_t y) {
    GreyScale_Pixel** to_return =
        (GreyScale_Pixel**)malloc(3 * sizeof(GreyScale_Pixel*));
    for (uint32_t i = 0; i < 3; i++)
        to_return[i] = (GreyScale_Pixel*)malloc(3 * sizeof(GreyScale_Pixel));

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if ((int32_t)(x + i) >= 0 && x + i < input->number_of_rows &&
                (int32_t)(y + j) >= 0 &&
                (uint32_t)(y + j) < input->pixels_per_row) {
                to_return[i + 1][j + 1] = input->data[x + i][y + j];
            } else {
                to_return[i + 1][j + 1] = 0;
            }
        }
    }

    return to_return;
}

/**
 * @brief Compute the convolution value for one pixel
 *
 * @author tanguy.baltazart
 * @param mask The convolution matrix
 * @param extract 3 * 3 image
 * @return int32_t
 */
int32_t SumMatrix(const float mask[3][3], GreyScale_Pixel** extract) {
    double sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            sum += mask[i][j] * extract[i][j];
        }
    }

    return (sum < 0.0) ? 0 : (sum > 255.0) ? 255 : (int)sum;
}

/**
 * @brief Convolute an image with an convolution matrix
 *
 * @author tanguy.baltazart
 * @param input The image to convolute
 * @param mask The convolution matrix
 * @return GreyScale_Image*
 */
GreyScale_Image* Convolute(const GreyScale_Image* input,
                           const float mask[3][3]) {
    GreyScale_Image* to_return =
        (GreyScale_Image*)malloc(sizeof(GreyScale_Image));
    to_return->number_of_rows = input->number_of_rows;
    to_return->pixels_per_row = input->pixels_per_row;
    to_return->number_of_pixels = input->pixels_per_row * input->number_of_rows;
    to_return->data = (GreyScale_Pixel**)malloc(input->number_of_rows *
                                                sizeof(GreyScale_Pixel*));

    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        to_return->data[i] = (GreyScale_Pixel*)malloc(input->pixels_per_row *
                                                      sizeof(GreyScale_Pixel));
    }

    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            GreyScale_Pixel** extract = Extract(input, i, j);

            to_return->data[i][j] = SumMatrix(mask, extract);
        }
    }
    return to_return;
}

/**
 * @brief Copies an GreyScale_Image
 * @author daniel.frederic
 * @param input The image to copy
 * @return GreyScale_Image* The copied image
 */
GreyScale_Image* copy(GreyScale_Image* input) {
    GreyScale_Image* to_return =
        (GreyScale_Image*)malloc(sizeof(GreyScale_Image));
    to_return->number_of_rows = input->number_of_rows;
    to_return->pixels_per_row = input->pixels_per_row;
    to_return->number_of_pixels = input->pixels_per_row * input->number_of_rows;
    to_return->data = (GreyScale_Pixel**)malloc(input->number_of_rows *
                                                sizeof(GreyScale_Pixel*));
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        to_return->data[i] = (GreyScale_Pixel*)malloc(input->pixels_per_row *
                                                      sizeof(GreyScale_Pixel));
    }

    for (size_t i = 0; i < (size_t)(to_return)->number_of_rows; i++)
        for (size_t y = 0; y < (size_t)(to_return)->pixels_per_row; y++)
            to_return->data[i][y] = input->data[i][y];
    return to_return;
}

/**
 * @brief Copies an BMP_Image
 * @author daniel.frederic
 * @param source The image to copy
 * @return BMP_Image* The copied image
 */
BMP_Image* copyColor(BMP_Image* source) {
    BMP_Image* to_return = (BMP_Image*)malloc(sizeof(BMP_Image));
    to_return->number_of_rows = source->number_of_rows;
    to_return->pixels_per_row = source->pixels_per_row;
    to_return->number_of_pixels =
        source->pixels_per_row * source->number_of_rows;
    to_return->data =
        (BMP_Pixel**)malloc(source->number_of_rows * sizeof(BMP_Pixel*));
    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        to_return->data[i] =
            (BMP_Pixel*)malloc(source->pixels_per_row * sizeof(BMP_Pixel));
    }

    for (size_t i = 0; i < (size_t)(to_return)->number_of_rows; i++)
        for (size_t y = 0; y < (size_t)(to_return)->pixels_per_row; y++)
            to_return->data[i][y] = source->data[i][y];
    return to_return;
}

/**
 * @brief Compute high pass filter for an image
 *
 * @authors tanguy.baltazart daniel.frederic
 * @param input The image to convolute
 * @return GreyScale_Image*
 */
GreyScale_Image* HighPass(const GreyScale_Image* input) {
    static float matrix[3][3] = {
        {0.0f, -0.15f, 0.0f}, {-0.15f, 2.0f, -0.15f}, {0.0f, -0.15f, 0.0f}};
    return Convolute(input, matrix);
}

/**
 * @brief Invert the color of the given image
 *
 * @author tanguy.baltazart
 * @param input The image wich color will be inverted.
 */
void Invert(GreyScale_Image* input) {
    for (size_t i = 0; i < input->pixels_per_row; i++) {
        for (size_t j = 0; j < input->number_of_rows; j++) {
            /* code */
            input->data[j][i] = 255 - input->data[j][i];
        }
    }
}

/**
 * @brief Apply edge detect filter to an image.
 *
 * @author tanguy.baltazrt
 * @param input The image which will have the filter apply to.
 * @return GreyScale_Image*
 */
GreyScale_Image* EdgeDetect(const GreyScale_Image* input) {
    static float matrix[3][3] = {
        {1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {-1.0f, 0.0f, 1.0f}};

    return Convolute(input, matrix);
}

/**
 * @brief Blur an image
 *
 * @author tanguy.baltazart
 * @param input The image to blur
 * @return GreyScale_Image*
 */
GreyScale_Image* Blur(const GreyScale_Image* input) {
    static float matrix[3][3] = {
        {(float)1 / 9, (float)1 / 9, (float)1 / 9},
        {(float)1 / 9, (float)1 / 9, (float)1 / 9},
        {(float)1 / 9, (float)1 / 9, (float)1 / 9},
    };

    return Convolute(input, matrix);
}
/**
 * @brief Apply the sobel filter to an image
 *
 * @author tanguy.baltazart
 * @param input The image which will have the filter apply to.
 * @return GreyScale_Image*
 */
GreyScale_Image* Sobel(const GreyScale_Image* input) {
    static float matrix1[3][3] = {
        {(float)1, (float)0, (float)-1},
        {(float)2, (float)0, (float)-2},
        {(float)1, (float)0, (float)-1},
    };
    static float matrix2[3][3] = {
        {(float)1, (float)2, (float)1},
        {(float)0, (float)0, (float)0},
        {(float)-1, (float)-2, (float)-1},
    };

    return Convolute(Convolute(input, matrix1), matrix2);
}

/**
 * @brief All pixel with value above the treshold will be white
 *
 * @author tanguy.baltazart
 * @param input The image to apply the treshold
 * @param tresh The value of the treshold
 */
void ApplyTreshHold(GreyScale_Image* input, int tresh) {
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            if (input->data[i][j] > tresh) {
                input->data[i][j] = 255;
            } else {
                input->data[i][j] = 0;
            }
        }
    }
}

void SteppingTreshHold(GreyScale_Image* input, int tresh) {
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            int buffer = 0;
            if (input->data[i][j] % tresh > tresh / 2) {
                buffer =
                    input->data[i][j] + tresh - (input->data[i][j] % tresh);
                if (buffer > 255)
                    buffer = 255;

            } else {
                buffer = input->data[i][j] - (input->data[i][j] % tresh);
                if (buffer < 0)
                    buffer = 0;
            }
            input->data[i][j] = (GreyScale_Pixel)buffer;
        }
    }
}

/**
 * @brief Save a GreyScaleImage in a file
 *
 * @authors daniel.frederic tanguy.baltazart
 * @param file_name The name of the file
 * @param input The image to save
 */
void BMP_SAVE(const char* file_name, GreyScale_Image* input) {
    FILE* image;

    int image_size = input->number_of_pixels;
    int file_size = 54 + 4 * image_size;

    struct __attribute__((packed)) bitmap_file_header {
        unsigned char bitmap_type[2];
        int file_size;
        short reserved1;
        short reserved2;
        unsigned int offset_bits;
    } bfh;

    // bitmap image header (40 bytes)
    struct bitmap_image_header {
        unsigned int size_header;
        unsigned int width;
        unsigned int height;
        short int planes;
        short int bit_count;
        unsigned int compression;
        unsigned int image_size;
        unsigned int ppm_x;
        unsigned int ppm_y;
        unsigned int clr_used;
        unsigned int clr_important;
    } bih;
    // unsigned char buff[14];

    // bfh[0] = 'B';
    // bfh[1] = 'M';
    // bfh[2] = file_size & 0x0F00;
    // bfh[3] = file_size & 0x00F0;
    // bfh[4] = file_size & 0x000F;

    // bfh[0x0a] = 0x3;
    // bfh[0x0b] = 0x6;



    memcpy(&bfh.bitmap_type, "BM", 2);
    bfh.file_size = file_size;
    bfh.reserved1 = 0;
    bfh.reserved2 = 0;
    bfh.offset_bits = 0x36;
    
    /*sprintf(buff, "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", bfh.bitmap_type[0], bfh.bitmap_type[1],
        (uint8_t)((bfh.file_size&0xFF)),  (uint8_t)((bfh.file_size&0xFF000000)>>3*8), (uint8_t)((bfh.file_size&0xFF0000)>>2*8), (uint8_t)((bfh.file_size&0xFF00)>>8), (uint8_t)((bfh.reserved1&0xFF00)>>8),
        (uint8_t)((bfh.reserved1&0xFF)),  (uint8_t)((bfh.reserved2&0xFF00)>>8), (uint8_t)(bfh.reserved2&0xFF), 
         (uint8_t)(bfh.offset_bits&0xFF) ,(uint8_t)((bfh.offset_bits&0xFF00)>>8), (uint8_t)((bfh.offset_bits&0xFF0000)>>2*8), (uint8_t)((bfh.offset_bits&0xFF000000)>>3*8));
    */
    bih.size_header = sizeof(bih);
    bih.width = input->pixels_per_row;
    bih.height = input->number_of_rows;
    bih.planes = 1;
    bih.bit_count = 24;
    bih.compression = 0;
    bih.image_size = file_size;
    bih.clr_used = 0;
    bih.clr_important = 0;

    image = fopen(file_name, "wb");

    fwrite(&bfh, 1, 14, image);
    fwrite(&bih, 1, sizeof(bih), image);
    char temp = 0;

    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            uint8_t red = (input->data[i][j]);
            uint8_t green = (input->data[i][j]);
            uint8_t blue = (input->data[i][j]);
            uint8_t color[3] = {blue, green, red};

            fwrite(&color, 1, sizeof(color), image);
        }
        for (uint32_t h = input->pixels_per_row % 4; h > 0; h--)
            fwrite(&temp, 1, 1, image);
    }

    fclose(image);
}

/**
 * @brief Save a BMP_image in file
 *
 * @authors daniel.frederic tanguy.baltazart
 * @param file_name The name of the file
 * @param input The image to save
 */
void SAVE_IMAGE(const char* file_name, BMP_Image* input) {
    FILE* image;

    int image_size = input->number_of_pixels;

    // int file_size = 54 + 4 * image_size;
    int file_size = (input->pixels_per_row%4)*(input->number_of_rows) + (3*image_size) + 54;

    // int ppm = 1200 * 39.375;

    struct __attribute__((packed)) bitmap_file_header {
        unsigned char bitmap_type[2];
        int32_t file_size;
        short reserved1;
        short reserved2;
        uint32_t offset_bits;
    } bfh;

    struct bitmap_image_header {
        unsigned int size_header;
        unsigned int width;
        unsigned int height;
        short int planes;
        short int bit_count;
        unsigned int compression;
        unsigned int image_size;
        unsigned int ppm_x;
        unsigned int ppm_y;
        unsigned int clr_used;
        unsigned int clr_important;
    } bih;
    // char buff[14];



    // buff[0] = 'B';
    // buff[1] = 'M';
    // buff[2] = (file_size) & 0xFF;
    // buff[3] = (file_size >>8) & 0xFF;
    // buff[4] = (file_size >>16) & 0xFF;
    // buff[5] = (file_size >>24) & 0xFF;

    // for(size_t i = 0x6; i < 0x0a; i++)
    //     buff[i] = 0x0;

    // buff[0x0a] = 0x36;
    // buff[0x0b] = 0x0;
    // buff[0x0c] = 0x0;
    // buff[0x0d] = 0x0;

     memcpy(&bfh.bitmap_type, "BM", 2);
    bfh.file_size = file_size;
    bfh.reserved1 = 0;
    bfh.reserved2 = 0;
    bfh.offset_bits = 0x36;

    /*
    sprintf(buff, "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", bfh.bitmap_type[0], bfh.bitmap_type[1],
        (uint8_t)((bfh.file_size&0xFF)),  (uint8_t)((bfh.file_size&0xFF000000)>>3*8), (uint8_t)((bfh.file_size&0xFF0000)>>2*8), (uint8_t)((bfh.file_size&0xFF00)>>8), (uint8_t)((bfh.reserved1&0xFF00)>>8),
        (uint8_t)((bfh.reserved1&0xFF)),  (uint8_t)((bfh.reserved2&0xFF00)>>8), (uint8_t)(bfh.reserved2&0xFF), 
         (uint8_t)(bfh.offset_bits&0xFF) ,(uint8_t)((bfh.offset_bits&0xFF00)>>8), (uint8_t)((bfh.offset_bits&0xFF0000)>>2*8), (uint8_t)((bfh.offset_bits&0xFF000000)>>3*8));
    */
    bih.size_header = sizeof(bih);
    bih.width = input->pixels_per_row;
    bih.height = input->number_of_rows;
    bih.planes = 1;
    bih.bit_count = 24;
    bih.compression = 0;
    bih.image_size = file_size;

    bih.clr_used = 0;
    bih.clr_important = 0;

    image = fopen(file_name, "wb");

    fwrite(&bfh, 1, sizeof(bfh), image);
    fwrite(&bih, 1, sizeof(bih), image);
    char temp[1];

    for (uint32_t i = 0; i < input->number_of_rows; i++) {

        for (uint32_t j = 0; j < input->pixels_per_row; j++) {

            uint8_t red = (input->data[i][j].R);
            uint8_t green = (input->data[i][j].G);
            uint8_t blue = (input->data[i][j].B);
            uint8_t color[3] = {blue, green, red};

            fwrite(color, 1, sizeof(color), image);
        }
        for (int h = input->pixels_per_row % 4; h > 0; h--)
            fwrite(temp, 1, 1, image);
    }

    fclose(image);
}
// void *MergeRegion(Vector_region *region)
// {
//     if (region == NULL)
//         return NULL;

//     Vector_region *newLoc = malloc(sizeof(Vector_region));
//     newLoc->next = NULL;

//     Vector_region *prev = region;
//     for (region = region->next; region->next != NULL;)
//     {
//         if (prev->region.confidence < region->region.confidence &&
//         region->region.y - (prev->region.y + prev->region.height) < 3)
//         {
//             prev->region.y += region->region.y;
//             prev->region.height += region->region.height + region->region.y -
//             (prev->region.y + prev->region.height); Vector_region *temp =
//             region->next; free(region); region = temp;
//         }
//         else
//         {
//             prev = region;
//             region = region->next;
//         }
//     }
// }
/**
 * @brief Get the line of an image.
 *
 * @author tanguy.baltazart
 * @param input the image to get the line from
 * @return vector_Region
 */
vector_Region GetProjectionProfile(const GreyScale_Image* input) {
    int min = input->pixels_per_row + 1;
    int max = 0;
    int* profile = (int*)malloc(input->number_of_rows * (sizeof(int)));
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        profile[i] = 0;
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            profile[i] += (input->data[i][j] > 65) ? 1 : 0;
        }

        if (profile[i] > max)
            max = profile[i];
        if (profile[i] < min)
            min = profile[i];
    }

    int diff = (max - min) / 15;
    int noiseValue = (max - min) / 20;
    vector_Region localisation = vector_of_Region(0);

    // int datacount = 0;
    int start = 0;
    // int lastScore = diff;
    for (uint32_t i = 0; i < input->number_of_rows - 1; i++) {
        // if (profile[i] < )
        if (profile[i] > noiseValue || profile[i] > diff ||
            (profile[i] > diff * 0.6 && profile[i] > diff * 0.6)) {
            if (start == -1)
                start = i;
        } else if (start != -1) {
            Region region;
            region.x = 0;
            region.y = start;
            region.width = input->pixels_per_row - 1;
            region.height = i - start;
            push_back_for_Region(&localisation, region);
            start = -1;
        }
    }
    // MergeRegion(localisation);
    return localisation;
}

/**
 * @brief Get the character in an line
 *
 * @author tanguy.baltazart
 * @param input The image to search the character in
 * @param region The region of the line.
 * @return vector_Region
 */
vector_Region GetColumn(const GreyScale_Image* input, const Region* region) {
    int min = region->height + 1;
    int max = 0;
    int* profile = (int*)malloc(input->pixels_per_row * (sizeof(int)));

    vector_Region a = vector_of_Region(0);

    for (uint32_t i = region->x; i < input->pixels_per_row; i++) {
        profile[i] = 0;
        for (uint32_t j = region->y; j < region->y + region->height; j++) {
            profile[i] += (input->data[j][i] > 20) ? 1 : 0;
        }

        if (profile[i] > max)
            max = profile[i];
        if (profile[i] < min)
            min = profile[i];
    }

    // int diff = (max - min) / 15;
    float noiseValue = (max - min) / ((region->height + 1) / 6.2);
    // float noiseValue = (max - min) / ((region->height + 1) / 5);

    // int datacount = 0;
    int start = 0;
    // int lastScore = diff;

    for (uint32_t i = 0; i < input->pixels_per_row; i++) {
        if ((float)profile[i] > noiseValue) {
            if (start == -1)
                start = i;
        } else if (start != -1) {
            Region maregion;
            maregion.x = start;
            maregion.y = region->y;
            maregion.height = region->height;
            maregion.width = i - start;
            push_back_for_Region(&a, maregion);
            start = -1;
        }
    }

    return a;
    // MergeRegion(localisation);
}

/**
 * @brief Check if there is white pixel outside the region
 *
 * @author tanguy.baltazart
 * @param input The image
 * @param region The region to check
 * @param treshold The value of the pixel to be considred white
 * @return true if there is a white outside the region
 */
bool verifTop(const GreyScale_Image* input, Region region, int treshold) {
    if (region.y <= 0) {
        return false;
    }
    for (uint32_t i = region.x; i < region.x + region.width; i++) {
        if (input->data[region.y - 1][i] > treshold)
            return true;
    }
    return false;
}

/**
 * @brief Check if there is white pixel outside the bottom of region
 *
 * @author tanguy.baltazart
 * @param input The image
 * @param region The region to check
 * @param treshold The value of the pixel to be considred white
 * @return true if there is a white outside the region
 */
bool verifBot(const GreyScale_Image* input, Region region, int treshold) {
    if (region.y + region.height +1 >= input->number_of_rows) {
        return false;
    }
    for (uint32_t i = region.x; i < region.x + region.width && i < input->number_of_rows; i++) {
        if (input->data[region.y + region.height + 1][i] > treshold)
            return true;
    }
    return false;
}

/**
 * @brief Check if there is white pixel adjacent to another white pixel at the
 * right of region
 *
 * @author tanguy.baltazart
 * @param input The image
 * @param region The region to check
 * @param treshold The value of the pixel to be considred white
 * @return true if there is a white outside the region
 */
bool verifRight(const GreyScale_Image* input, Region region, int treshold) {
    if (region.x + region.width >= input->pixels_per_row) {
        return false;
    }
    for (uint16_t i = region.y; i < region.y + region.height; i++) {
        if (input->data[i][region.width + region.x + 1] > treshold &&
            input->data[i][region.width + region.x] > treshold)
            return true;
    }
    return false;
}

// bool verifLeft(const GreyScale_Image* input, Region region, int treshold) {
//     if (region.x <= 0) {
//         return false;
//     }
//     for (int i = region.y; i < region.y + region.height; i++) {
//         if (input->data[i][region.x - 1] > treshold &&
//             input->data[i][region.x] > treshold)
//             return true;
//     }
//     return false;
// }

/**
 * @brief Expands regions in list
 * @authors daniel.frederic tanguy.baltazart
 * @param input The image
 * @param list The list of Region
 * @return vector_Region The modified regions
 */
vector_Region Expand(const GreyScale_Image* input, vector_Region list) {
    const int offset = 80;
#define droite(i) nth(list, i).x + nth(list, i).width
#define gauche(i) nth(list, i).x

    for (size_t i = 0; i < list.element_count; i++) {

        if (nth(list, i).width == 0) {
            continue;
            Remove_at_for_Region(&list, i);
        }
        while (verifTop(input, nth(list, i), offset)) {
            // nth(list, i).y--;
            nth(list, i).y--;
            nth(list, i).height++;
        }

        while (verifBot(input, nth(list, i), offset)) {
            // nth(list, i).height++;
            nth(list, i).height++;
        }

        while (verifRight(input, nth(list, i), offset)) {
            nth(list, i).width++;
            if (i < list.element_count + 1) {
                if (droite(i) == gauche(i + 1)) {
                    nth(list, i).width += nth(list, i + 1).width;
                    Remove_at_for_Region(&list, i + 1);
                }
            }
            // nth(list, i).width++;
        }

#undef droite
#undef gauche

        // while (verifLeft(input, nth(list, i), offset))
        // {

        //     // nth(list, i).x--;
        //     nth(list, i).x--;
        //     nth(list,i).width++;

        // }
    }
    return list;
}

/**
 * @brief Draw a box in an image
 *
 * @author tanguy.baltazart
 * @param input The image to draw the box in
 * @param region the region of the box
 */
void DrawRegion(const GreyScale_Image* input, const Region* region) {
    if (region->x + region->width < input->pixels_per_row &&
        region->y + region->height < input->number_of_rows) {
        for (uint32_t i = region->y; i < region->height + region->y; i++) {
            for (uint32_t j = region->x; j < region->width + region->x; j++) {
                input->data[i][j] += DIMENSION * (input->data[i][j] <= 205);
            }
        }
    }
}

/**
 * @brief Extract a region of pixel from an image and create an new image
 *
 * @author tanguy.baltazart
 * @param input the image to extract from
 * @param region the region who need to be extracted
 * @return GreyScale_Image*
 */
GreyScale_Image* ExtractRegion(const GreyScale_Image* input,
                               const Region* region) {

    GreyScale_Image* to_return =
        (GreyScale_Image*)malloc(sizeof(GreyScale_Image));

    to_return->number_of_rows = region->height + 1;
    to_return->pixels_per_row = region->width + 1;
    to_return->number_of_pixels = region->height + 1 * region->width + 1;

    to_return->data = (GreyScale_Pixel**)malloc(to_return->number_of_rows *
                                                sizeof(GreyScale_Pixel*));
    for (uint32_t i = 0; i < to_return->number_of_rows; i++) {
        to_return->data[i] = (GreyScale_Pixel*)malloc(
            to_return->pixels_per_row * sizeof(GreyScale_Pixel));
    }

    for (uint32_t i = region->y; i < to_return->number_of_rows + region->y;
         i++) {
        for (uint32_t j = region->x; j < to_return->pixels_per_row + region->x;
             j++) {
            to_return->data[i - region->y][j - region->x] = input->data[i][j];
        }
    }
    return to_return;
}

/**
 * @brief Convert a degrees to a radian
 * @author  nicolas.kourban
 * @param degrees a number that represent the angle in degrees(double)
 * @return double the converted angle in radian
 */

double DegreesToRadians(double degrees) {
    double radians = degrees * PI / 180;
    return radians;
}

/**
 * @brief search for the index of the maximum in an array
 * @author nicolas.kourban
 * @param vecteur array
 * @return int the maximum index
 */

int argmax(vector_vector_int vecteur) {

    int row = 0;
    int col = 0;
    for (size_t y = 0; y < vecteur.element_count; y++) {
        for (int x = 0; x < 180; x++) {
            if (nth2(vecteur, y, x) > nth2(vecteur, row, col)) {
                // int val = nth2(vecteur, y, x;
                row = y;
                col = x;
            }
        }
    }
    return row * 180 + (col);
}

/**
 * @brief Find the angle of rotation of a GreyScale_Image
 * @author nicolas.kourban
 * @param input  A GreyScale_Image
 * @return double the angle in degrees
 */
double GetAngle(GreyScale_Image* input) {

    int Maxdist = round((sqrt(input->pixels_per_row * input->pixels_per_row +
                              input->number_of_rows * input->number_of_rows)));
    vector_double thetas = init_vector_double(180, 0);
    vector_int rs = init_vector_int(2 * Maxdist, 0);
    vector_vector_int accumulateur = vector_of_vector_int(0);

    for (int i = 0; i < 2 * Maxdist; i++) {
        push_back_for_vector_int(&accumulateur,
                                 init_vector_int(thetas.element_count, 0));
    }
    for (int i = 0; i <= 180; i++) {
        nth(thetas, i) = DegreesToRadians(i - 90);
    }
    for (int i = 0; i < 2 * Maxdist; i++) {
        nth(rs, i) = i - Maxdist;
    }
    for (uint32_t y = 0; y < input->number_of_rows; y++) {
        for (uint32_t x = 0; x < input->pixels_per_row; x++) {

            if (input->data[y][x] > 10) {
                for (size_t k = 0; k < thetas.element_count; k++) {
                    double r =
                        x * cos(nth(thetas, k)) + y * sin(nth(thetas, k));
                    nth2(accumulateur, (int)r + Maxdist, k) += 1;
                }
            }
        }
    }
    // double theta =(nth(thetas, ((int)(argmax(accumulateur) % 180))) * 180 /
    // PI);
    double angle = -((nth(thetas, (argmax(accumulateur)) % 180)) * 180 / PI);
    free_tensor2(accumulateur);
    delete_vec(thetas);
    delete_vec(rs);
    if (angle >= 89 && angle <= 91) {
        angle = 0;
    }
    if (angle < 0) {
        angle = -90 + (-angle);
    }

    return angle;
}

/**
 * @brief calculate the absolute value
 * @author nicolas.kourban
 * @param n a number
 * @return float the absolute value of the number
 */
float abso(float n) {
    if (n < 0) {
        return (-n);
    }
    return n;
}

/**
 * @brief Rotate the GreyScale_Image by the specified angle
 * @author nicolas.kourban
 * @param input A GresScale_Image*
 * @param angle the angle we need to apply
 * @return GreyScale_Image* rotated
 */

GreyScale_Image* RotatebyAngle(GreyScale_Image* input, double angle) {
    if (angle == 0) {
        return copy(input);
    }
    angle *= PI / 180.0;
    float sinus = (sin(abso(angle)));
    float cosinus = (cos(abso(angle)));
    float newsin = (sin(angle));
    float newcos = (cos(angle));
    double newImgWidth =
        sinus * input->number_of_rows + cosinus * input->pixels_per_row;
    double newImgHeight =
        sinus * input->pixels_per_row + cosinus * input->number_of_rows;
    GreyScale_Image* rotated =
        Create_Image((int)newImgWidth + 1, (int)newImgHeight + 1, 255);
    int centerraw = input->number_of_rows / 2 + 1;
    int centercol = input->pixels_per_row / 2 + 1;
    for (int32_t y = 0; y < (int32_t)input->number_of_rows - 1; y++) {
        for (int32_t x = 0; x < (int32_t)input->pixels_per_row - 1; x++) {
            // float distance = sqrt((y - centerraw) * (y - centerraw) +
            //                    (centercol - x) * (centercol - x));
            double newx = (newcos * (x - centercol) + newsin * (y - centerraw));
            double newy =
                ((-1.0) * newsin * (x - centercol) + newcos * (y - centerraw));
            rotated->data[(int32_t)(newy + newImgHeight / 2)]
                         [(int32_t)(newx + newImgWidth / 2)] =
                input->data[y][x];
        }
    }
    return rotated;
}


#ifdef DANIEL_RESIZE_BICUBIC_INTERPOLATION

#ifndef uchar
#define uchar uint8_t
#endif
#define width pixels_per_row
#define height number_of_rows
#define widthStep pixels_per_row
GreyScale_Image* Resize(GreyScale_Image* img, uint16_t newWidth, uint16_t newHeight){
    GreyScale_Image* to_return = Create_Image(newWidth, newHeight, 0);
    uchar** data = img->data;
    uchar** Data = to_return->data;
    int a,b,c,d,index;
    uchar Ca,Cb,Cc;
    uchar C[5];
    uchar d0,d2,d3,a0,a1,a2,a3;
    int i,j,k,ii,jj;
    int x,y;
    float dx,dy;
    float tx,ty;
    tx = (float)img->width / newWidth ;
    ty =  (float)img->height / newHeight;
    for(i=0; i<newHeight; i++)
        for(j=0; j<newWidth; j++)
        {
                  // printf("%d : %d\n",i,j);
           x = (int)(tx*j);
           y =(int)(ty*i);
           
           dx= tx*j-x;
           dy= ty*i -y;
           
           
           index = y*1 + x*1 ;
           a = y*1 + (x+1)*1 ;
           b = (y+1)*1 + x*1 ;
           c = (y+1)*1 + (x+1)*1 ;

           if(x == -1 || x == 0) x = 1;
           if(y == -1 || y == 0) y = 1;

           for(k=0;k<3;k++)
           {
              for(jj=0;jj<=3;jj++)
              {
                    if(y+jj > img->height) y = img->height-1-jj;
                    if(x+k > img->width) x = img->width-1-k;
                 d0 = data[(y+jj)*1][(x-1)*1 +k] - data[(y+jj)*1][(x)*1 +k] ;
                 d2 = data[(y+jj)*1][(x+1)*1 +k] - data[(y+jj)*1][(x)*1 +k] ;
                 d3 = data[(y+jj)*1][(x+2)*1 +k] - data[(y+jj)*1][(x)*1 +k] ;
                 a0 = data[(y+jj)*1][(x)*1 +k];
                 a1 =  -1.0/3*d0 + d2 -1.0/6*d3;
                 a2 = 1.0/2*d0 + 1.0/2*d2;
                 a3 = -1.0/6*d0 - 1.0/2*d2 + 1.0/6*d3;
                 C[jj] = a0 + a1*dx + a2*dx*dx + a3*dx*dx*dx;
                 
                 d0 = C[0]-C[1];
                 d2 = C[2]-C[1];
                 d3 = C[3]-C[1];
                 a0=C[1];
                 a1 =  -1.0/3*d0 + d2 -1.0/6*d3;
                 a2 = 1.0/2*d0 + 1.0/2*d2;
                 a3 = -1.0/6*d0 - 1.0/2*d2 + 1.0/6*d3;
                 Cc = a0 + a1*dy + a2*dy*dy + a3*dy*dy*dy;
                
                 Data[i][j +k ] = Cc;
              }
           }
           
        }
        return to_return;
}
#undef uchar
#undef width
#undef height
#undef widthStep

#else

GreyScale_Image *Resize(GreyScale_Image *img, double newWidth, double newHeight){
        GreyScale_Image* img2 =
        Create_Image((uint32_t)newWidth, (uint32_t)newHeight, 255);
        float tx = (float)(img->pixels_per_row-1) /newWidth ;
        float ty =  (float)(img->number_of_rows-1) / newHeight;
        for(int i=0;i<newHeight;i++){
            for(int j=0;j<newWidth;j++)
            {
                int x = ceil(tx*j);
                int y =ceil(ty*i);
                for(int k=0;k<3;k++)
                {
                    int indexa = j+k;
                    int indexb = x+k;
                    //if(indexa >= newWidth) indexa = newWidth - 1;
                    //if(indexb >= img->pixels_per_row) indexb = img->pixels_per_row - 1;
                    img2->data[i][indexa] = img->data[y][indexb%img->pixels_per_row];     
                }
            }
        }
        return img2;
}
#endif

/**
 * @brief clean the image
 * @author nicolas.kourban
 * @param input A GresScale_Image*
 */
void Clean_image1(GreyScale_Image* input) {
    for (size_t y = 1; y < input->number_of_rows - 1; y++) {
        for (size_t x = 1; x < input->pixels_per_row - 1; x++) {
            int polette = input->data[y + 1][x] + input->data[y][x + 1] +
                          input->data[y - 1][x] + input->data[y][x - 1];
            if (input->data[y][x] == 0 && polette >= 510) {
                input->data[y][x] = 255;
            }
        }
    }
}
/**
 * @brief clean the image
 * @author nicolas.kourban
 * @param input A GresScale_Image*
 */
void Clean_image2(GreyScale_Image* input) {
    for (size_t y = 1; y < input->number_of_rows - 1; y++) {
        for (size_t x = 1; x < input->pixels_per_row - 1; x++) {
            int polette = input->data[y + 1][x] + input->data[y][x + 1] +
                          input->data[y - 1][x] + input->data[y][x - 1];
            if (input->data[y][x] == 255 && polette <= 510) {
                input->data[y][x] = 0;
            }
        }
    }
}

/**
 * @brief check if b is included in a
 *
 * @param a the biggest region
 * @param b the smallest region.
 * @author tanguy.baltazart
 */
bool IsRegionIncluded(Region a, Region b) {
    if (b.y > a.y && (b.y + b.height) < (a.y + a.height)) {
        if (b.x > a.x && (b.x + b.width) < (a.x + a.width)) {
            return true;
        }
    }
    return false;
}

void clearIncluded(vector_Region* regList) {

    for (size_t i = 0; i < regList->element_count; i++) {
        for (size_t j = i + 1; j < regList->element_count; j++) {
            if (IsRegionIncluded(regList->data[i], regList->data[j])) {
                Remove_at_for_Region(regList, j);
                j--;
            }
        }
        /* code */
    }
}

/**
 * @brief Draw a box at the region location.
 *
 * @param source The image to draw on
 * @param region The region to draw.
 * @param r The red component of the pixel.
 * @param g The green component of the pixel.
 * @param b The blue component of the pixel.
 * @author tanguy.baltazart
 */

void DrawColorRegion(BMP_Image* source, Region* region, uint8_t r, uint8_t g,
                     uint8_t b) {

    BMP_Pixel color = {r, g, b};

    for (size_t j = region->x;
         j < region->x + region->width &&
         region->x + region->width < source->pixels_per_row;
         j++) {
        source->data[region->y][j] = color;
        source->data[region->y + region->height][j] = color;
    }

    for (size_t i = region->y;
         i < region->y + region->height &&
         region->y + region->height < source->number_of_rows;
         i++) {
        source->data[i][region->x] = color;
        source->data[i][region->x + region->width] = color;
    }
}

/**
 * @brief Draw a box at the region location.
 *
 * @param source The image to draw on
 * @param regions The list of region to draw.
 * @param r The red component of the pixel.
 * @param g The green component of the pixel.
 * @param b The blue component of the pixel.
 * @author tanguy.baltazart
 */
void DrawColorVectorRegion(BMP_Image* source, vector_Region* regions, uint8_t r,
                           uint8_t g, uint8_t b) {
    for (size_t i = 0; i < regions->element_count; i++) {
        DrawColorRegion(source, &(regions->data[i]), r, g, b);
    }
}

void ProjectionDetectChar(BMP_Image* source, GreyScale_Image* grey) {

    vector_Region regions = GetProjectionProfile(grey);

    for (size_t j = 0; j < regions.element_count; j++) {
        if (nth(regions, j).height == 0) {
            continue;
        }
        vector_Region column = Expand(grey, GetColumn(grey, &nth(regions, j)));
        for (size_t i = 0; i < column.element_count; i++) {
            DrawColorVectorRegion(source, &column, 255, 0, 0);
        }
        delete_vec(column);
    }
    delete_vec(regions);
}

/**
 * @brief All pixel with value above the treshold will be white
 *
 * @author tanguy.baltazart
 * @param input The image to apply the treshold
 * @param tresh The value of the treshold
 */
void ApplyTreshHoldbin(GreyScale_Image* input, int tresh) {
    for (uint32_t i = 0; i < input->number_of_rows; i++) {
        for (uint32_t j = 0; j < input->pixels_per_row; j++) {
            if (input->data[i][j] < tresh) {
                input->data[i][j] = 1;
            } else {
                input->data[i][j] = 0;
            }
        }
    }
}



#define MAX(a, b) (a > b) ? a : b
/**
 * @brief Convert an MSER region into a GreyScale image the background of the image will be black and the foreground white.
 * 
 * @param region The region containing the id of the foreground pixel.
 * @param mask The mask of descrpitor for each pixel.
 * @return GreyScale_Image* 
 */
GreyScale_Image* RegToSquare(Region region, GreyScale_Image* source) {
    uint32_t width = region.width+1;
    uint32_t height = region.height+1;
    size_t size = MAX(width, height) + 10;
    GreyScale_Image* image = Create_Image(size, size, 0);

    size_t offset_i = (size - width) / 2;
    size_t offset_j = (size - height) / 2;

   

    for (size_t j = offset_j; j < size- offset_j; j++) {
        for (size_t i = offset_i; i < size- offset_i; i++) {
            size_t x = i - offset_i;
            size_t y = j - offset_j;
            image->data[j][i] =
                source->data[region.y + y][region.x + x];
        }
    }

    return image;
}
#undef MAX

#endif