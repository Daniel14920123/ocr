/**
 * @file tensor_def.h
 * @authors daniel.frederic nicolas.kourban tanguy.baltazart
 * @brief Implementing metafunctions for different types. Avoid repetition of them.
 * @version 0.1
 * @date 2020-10-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef TENSOR_DEF
#define TENSOR_DEF

#include "tensor.h"

VEC(int);
VEC(vector_int);
TENSOR_init1(size_t);
TENSOR_init3(float);
TENSOR_init2(double);

typedef tensor3(float) mlp;
typedef tensor2(float) nnexec;
#endif
