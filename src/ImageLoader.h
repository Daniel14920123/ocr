/**
 * @file ImageLoader.h
 * @author daniel.frederic
 * @brief Loading an image
 * @version 1.0
 * @date 2020-09-01
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef IMAGE_LOADER_HEADER_INCLUDED
#define IMAGE_LOADER_HEADER_INCLUDED

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/*
    READS BMP 3 BYTES FILES
*/

/**
 * @brief Pixel on a bmp image
 * @author daniel.frederic
 *
 */
typedef struct {
  uint8_t R;
  uint8_t G;
  uint8_t B;
} BMP_Pixel;

/**
 * @brief Bmp image
 * @author daniel.frederic
 * 
 */
typedef struct {
  BMP_Pixel** data;
  uint32_t pixels_per_row;
  uint32_t number_of_rows;
  uint32_t number_of_pixels;
} BMP_Image;

#define CUSTOM_BMP

#ifdef CUSTOM_BMP
/**
 * @brief Loads a bmp image from a file
 * 
 * @param filename 
 * @return BMP_Image* 
 */
BMP_Image* BMP_open(char* filename) {
    FILE* stream = fopen(filename, "rb");

    if(!stream)
        return NULL;
    // char buff[4];

    if (stream == NULL)
        return NULL;

    fseek(stream, 10, SEEK_SET);
    int start;
    fread(&start, 1, 4, stream);
    start += (start == 0)*0x36;
    //^- Warning with the 4 bytes implementation of the section
    BMP_Image* to_return = (BMP_Image*)malloc(sizeof(BMP_Image));
    fseek(stream, 0x12, SEEK_SET);
    int width;
    fread(&width, 1, 4, stream);
    //^- Warning with the implementation of the section
    // fseek(stream, 0x16, SEEK_SET);
    int height;
    fread(&height, 1, 4, stream);
    //^- Warning with the implementation of the section

    to_return->number_of_rows = height;
    to_return->pixels_per_row = width;
    to_return->number_of_pixels = width * height;
    to_return->data = (BMP_Pixel**)malloc(height * sizeof(BMP_Pixel*));
    for (int i = 0; i < height; i++) {
        to_return->data[i] = (BMP_Pixel*)malloc(width * sizeof(BMP_Pixel));
    }

    uint8_t buffer;
    fseek(stream, start, SEEK_SET);
    for (int32_t i = 0; i < height; i++) {
        for (int32_t j = 0; j < width; j++) {
        // printf("%i", fread(&buffer, 1, 1, stream));
        fread(&buffer, 1, 1, stream);
        to_return->data[i][j].B = buffer;
        fread(&buffer, 1, 1, stream);
        // printf("%i", fread(&buffer, 1, 1, stream));
        to_return->data[i][j].G = buffer;
        fread(&buffer, 1, 1, stream);
        // printf("%i", fread(&buffer, 1, 1, stream));
        to_return->data[i][j].R = buffer;
        // fread(&buffer, 1, 1, stream);
        // if (start == 138)
        //     fseek(stream, 4, SEEK_CUR);
        }
        for (int h = width % 4; h > 0; h--)
        fread(&buffer, 1, 1, stream);
    }

    fclose(stream);

    return to_return;
}
#else

#include <SDL/SDL.h>

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp)
    {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16 *)p;
            break;

        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
                break;

            case 4:
                return *(Uint32 *)p;
                break;

            default:
                return 0; 
      }
}



BMP_Image* BMP_open(char* filename){
    SDL_Surface * image = SDL_LoadBMP(filename);
    BMP_Image* to_return = (BMP_Image*)malloc(sizeof(BMP_Image));
    *to_return = (BMP_Image){
        .data = (BMP_Pixel**)malloc(image->h * sizeof(BMP_Pixel*)),
        .pixels_per_row = (uint32_t)image->w,
        .number_of_rows = (uint32_t)image->h,
        .number_of_pixels = (uint32_t)image->w*image->h,
    };
    for(size_t i = 0; i < image->h; i++){
        to_return->data[i] = (BMP_Pixel*)malloc(image->w * sizeof(BMP_Pixel));
        for(int y = 0; y < image->w; y++){
            Uint32 data = getpixel(image, i, y);
            SDL_GetRGB(data, image->format, &(to_return->data[i][y].R),
                                            &(to_return->data[i][y].G),
                                            &(to_return->data[i][y].B));
            printf("%d\t", to_return->data[i][y].R);
        }
        printf("\n");
    }

    SDL_Surface *ecran = NULL, *imageDeFond = NULL;
    SDL_Rect positionFond;

    positionFond.x = 0;
    positionFond.y = 0;


    SDL_Init(SDL_INIT_VIDEO);

    ecran = SDL_SetVideoMode(600, 600, 32, SDL_HWSURFACE);
    SDL_WM_SetCaption("Chargement d'images en SDL", NULL);

    /* Chargement d'une image Bitmap dans une surface */
    /* On blitte par-dessus l'écran */
    SDL_BlitSurface(image, NULL, ecran, &positionFond);

    SDL_Flip(ecran);

    SDL_FreeSurface(imageDeFond); /* On libère la surface */
    SDL_Quit();

    return to_return;
}

#endif


#endif
