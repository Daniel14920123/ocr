#ifndef DIMENSION_H_INCLUDED
#define DIMENSION_H_INCLUDED

/**
 * @brief dimension of images to compare. Sets variance among the images
 * 
 */
#define DIMENSION 150

/**
 * @brief determines the binarization positive value. Don't change anything
 * 
 */
#define EJECTION 1

/**
 * @brief Activates the KNN algorithm
 * 
 */
#define KNN

/**
 * @brief Sets the K hyperparameter. Increasing leads to more adaptativity, but requires more data.
 * 
 * 
 */
#define VOTE 3


#endif