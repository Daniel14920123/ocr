/**
 * @file vector.h
 * @author daniel.frederic
 * @brief Metaprogram that generates vector_based constructs,
 * with optimisation of allocations
 * @version 0.9
 * @date 2020-09-00
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef META_VEC_H_INCLUDED
#define META_VEC_H_INCLUDED

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief The generator for the type type. It provides vector_type, push pushing, 
 * access and deletion operations. It offers construction, access, freeing and
 * iteration on those structures
 *
 * 
 */
#define VEC(type)                                                              \
    typedef struct {                                                           \
        size_t element_count;                                                  \
        size_t allocated;                                                      \
        type* data;                                                            \
    } vector_##type;                                                           \
    vector_##type vector_of_##type(size_t size) {                              \
        vector_##type to_return;                                               \
        to_return.element_count = 0;                                           \
        to_return.allocated = size;                                            \
        to_return.data = (type*)malloc(size * sizeof(type));                   \
        return to_return;                                                      \
    }                                                                          \
    vector_##type init_vector_##type(size_t nb, type default_val) {            \
        vector_##type to_return;                                               \
        to_return.element_count = nb;                                          \
        to_return.allocated = nb + (5 - nb % 5);                               \
        to_return.data = (type*)malloc(to_return.allocated * sizeof(type));    \
        for (size_t i = 0; i < nb; i++) {                                      \
            to_return.data[i] = default_val;                                   \
        }                                                                      \
        return to_return;                                                      \
    }                                                                          \
    vector_##type __construct_of_##type(type v[], size_t size) {               \
        vector_##type to_return = vector_of_##type(size + 5);                  \
        to_return.element_count = size;                                        \
        for (size_t i = 0; i < size; i++) {                                    \
            to_return.data[i] = v[i];                                          \
        }                                                                      \
        return to_return;                                                      \
    }                                                                          \
    void push_back_for_##type(vector_##type* v, type value) {                  \
        if (v->allocated == v->element_count) {                                \
            v->allocated += 5;                                                 \
            v->data =                                                          \
                (type*)realloc(v->data, (v->allocated + 5) * sizeof(type));    \
        }                                                                      \
        v->element_count++;                                                    \
        v->data[v->element_count - 1] = value;                                 \
    }                                                                          \
    void pop_back_for_##type(vector_##type* v) { v->element_count--; }         \
    void iter_ref_for_##type(vector_##type* v, void (*fn)(type*)) {            \
        for (size_t i = 0; i < v->element_count; i++) {                        \
            (*fn)(&(v->data[i]));                                              \
        }                                                                      \
    }                                                                          \
    void iter_val_for_##type(vector_##type* v, void (*fn)(type)) {             \
        for (size_t i = 0; i < v->element_count; i++) {                        \
            (*fn)(v->data[i]);                                                 \
        }                                                                      \
    }                                                                          \
    void Remove_at_for_##type(vector_##type* v, size_t i) {                    \
        if (i < v->element_count) {                                            \
            for (; i < v->element_count - 1; i++) {                            \
                (v->data[i]) = (v->data[i + 1]);                                   \
            }                                                                  \
            v->element_count--;                                                \
        }                                                                      \
    }

// printf("Deleting "); printf(#to_free); printf("\n");
#define delete_vec(to_free) \
    (to_free).element_count = 0; (to_free).allocated = 0; free((to_free).data)
#define nth(v, i) (v).data[i]
#define nth2(v, row, col) (v).data[row].data[col]
#define nth3(v, row, col, z) (v).data[row].data[col].data[z]
#define vector_of_cc(type, ARRAY)                                              \
    (__construct_of_##type(ARRAY, sizeof(ARRAY) / sizeof(ARRAY[0])))
#define free_wi(v)                                                             \
    for (size_t i = 0; i < (v).allocated; i++)                                 \
        free((v).data[i]);                                                     \
    free((v).data)
#define free_tensor2(f) for(size_t i = 0; i < (f).allocated; i++){delete_vec(f.data[i]);} delete_vec(f);
#endif
