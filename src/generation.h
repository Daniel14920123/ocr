#pragma once
#include <stdio.h>
#include "pdf.h"

typedef enum {
    txt,
    pdf,
    html,
    odt
} fformat;

void write_to(const char* text, const char* filename, fformat ftype){
    char buffer[200];
    switch (ftype)
    {
    case txt:
        {
            sprintf(buffer, "%s.txt", filename);
            FILE* f = fopen(buffer, "w");
            fprintf(f, "%s", text);
            fclose(f);
            break;
        }
    case pdf:
        {
            sprintf(buffer, "%s.pdf", filename);
            struct pdf_info info = {
                .creator = "OCR User",
                .producer = "OCR by D.F. and T.B. and N.K.",
                .title = "OCR output",
                .author = "OCR User",
                .subject = "ocr output",
                .date = "Today"
            };
            struct pdf_doc *pdf = pdf_create(PDF_A4_WIDTH, PDF_A4_HEIGHT, &info);
            pdf_set_font(pdf, "Times-Roman");
            pdf_append_page(pdf);
            pdf_add_text(pdf, NULL, text, 12, 30, PDF_A4_HEIGHT-70, PDF_BLACK);
            pdf_save(pdf, buffer);
            pdf_destroy(pdf);
                    break;
        }
    case html:
        {
            sprintf(buffer, "%s.html", filename);
            FILE* f = fopen(buffer, "w");
            fprintf(f, "<html><head><title>OCR output</title></head><body><p>%s</p></body></html>", text);
            fclose(f);
            break;
        }
    case odt:
        {
            sprintf(buffer, "%s.rtf", filename);
            FILE* f = fopen(buffer, "w");
            fprintf(f, "{\\rtf1\\ansi\\deff3\\adeflang1025\n\
{\\fonttbl{\\f0\\froman\\fprq2\\fcharset0 Times New Roman;}\
{\\f1\\froman\\fprq2\\fcharset2 Symbol;}{\\f2\\fswiss\\fprq2\\fcharset0 Arial;}\
{\\f3\\froman\\fprq2\\fcharset0 Liberation Serif{\\*\\falt Times New Roman};}\
{\\f4\\fswiss\\fprq2\\fcharset0 Liberation Sans{\\*\\falt Arial};}\
{\\f5\\fnil\\fprq2\\fcharset0 Noto Sans CJK SC Regular;}\
{\\f6\\fnil\\fprq2\\fcharset0 FreeSans;}\
{\\f7\\fswiss\\fprq0\\fcharset128 FreeSans;}}\n\
{\\colortbl;\\red0\\green0\\blue0;\\red0\\green0\\blue\
255;\\red0\\green255\\blue255;\\red0\\green255\\blue0;\\red\
255\\green0\\blue255;\\red255\\green0\\blue0;\\red255\\green\
255\\blue0;\\red255\\green255\\blue255;\\red0\\green0\\blue\
128;\\red0\\green128\\blue128;\\red0\\green128\\blue0;\\red\
128\\green0\\blue128;\\red128\\green0\\blue0;\\red128\\green\
128\\blue0;\\red128\\green128\\blue128;\\red192\\green\
192\\blue192;}\n\
{\\stylesheet{\\s0\\snext0\\widctlpar\\hyphpar0\\c\
f0\\kerning1\\dbch\\af5\\langfe2052\\dbch\\af6\\af\
s24\\alang1081\\loch\\f3\\fs24\\lang1036 Normal;}\n\
{\\s15\\sbasedon0\\snext16\\sb240\\sa120\\keepn\\db\
ch\\af5\\dbch\\af6\\afs28\\loch\\f4\\fs28 Titre;}\n\
{\\s16\\sbasedon0\\snext16\\sl288\\slmult1\\sb0\\sa\
140 Corps de texte;}\n\
{\\s17\\sbasedon16\\snext17\\sl288\\slmult1\\sb0\\sa\
140\\dbch\\af7 Liste;}\n\
{\\s18\\sbasedon0\\snext18\\sb120\\sa120\\noline\\i\\d\
bch\\af7\\afs24\\ai\\fs24 L\\u233\\'e9gende;}\n\
{\\s19\\sbasedon0\\snext19\\noline\\dbch\\af7 Index;}\n\
}{\\*\\generator LibreOffice/5.1.6.2$Linux_X86_64 LibreOf\
fice_project/10m0$Build-2}{\\info{\\creatim\\yr2020\\mo12\\dy13\\hr12\\min50}{\\revtim\\yr2020\\mo12\\dy13\\hr14\\min36}"
"{\\printim\\yr0\\mo0\\dy0\\hr0\\min0}}\\deftab709\n\
\\viewscale100\n\
{\\*\\pgdsctbl\n\
{\\pgdsc0\\pgdscuse451\\pgwsxn11906\\pghsxn16838\\marglsxn\
1134\\margrsxn1134\\margtsxn1134\\margbsxn1134\\pgdsc\
nxt0 Style par d\\u233\\'e9faut;}}\n\
\\formshade\\paperh16838\\paperw11906\\margl1134\\marg\
r1134\\margt1134\\margb1134\\sectd\\sbknone\\sectunlock\
ed1\\pgndec\\pgwsxn11906\\pghsxn16838\\marglsxn1134\\mar\
grsxn1134\\margtsxn1134\\margbsxn1134\\ftnbj\\ftnsta\
rt1\\ftnrstcont\\ftnnar\\aenddoc\\aftnrstcont\\aftnst\
art1\\aftnnrlc\n\
{\\*\\ftnsep\\chftnsep}\\pgndec\\pard\\plain \\s0\\widc\
tlpar\\hyphpar0\\cf0\\kerning1\\dbch\\af5\\langfe2052\\d\
bch\\af6\\afs24\\alang1081\\loch\\f3\\fs24\\lang1036{\\rt\
lch \\ltrch\\loch\n\
%s}\n\
\\par }", 
                text);
            fclose(f);
            break;
        }
    default:
        {
            break;
        }
    }
}