#include "ImageLoader.h"
#include "ImageProcessing.h"
// #include "multilayerperceptron.h"
// #include "myneural.h"
#include "mser.h"
#include "tensor_def.h"
#include "training.h"
#include <stdio.h>
#include "OtherAlgorithmIntent.h"

// typedef PixelDescriptor** tamer;

#ifdef CONSTEXPR
#define MIN_VALUE 33
#define MAX_VALUE 65
#define CMP_BEGIN 60
#endif

#define IMG_NUMBER 1

//VEC(vector_RegionDescriptor)
//VEC(tamer)

#define delete2D(array, size) for(size_t i = 0; i < size;i++) free((array[i])); free(array);
/*
size_t getMinIndex(vector_vector_RegionDescriptor* vec) {
    size_t min = 0;

    for (size_t i = 0; i < vec->element_count; i++) {
        if (vec->data[i].element_count < vec->data[min].element_count)
            min = i;
    }

    return min;
}

void DefaultMser(BMP_Image* source, GreyScale_Image* grey) {
    PixelDescriptor** mask = GetPixelIdMask(grey);
    vector_RegionDescriptor MSRegion = GetRegionList(grey, mask, 20);

    drawMser(source, &MSRegion, mask);
    SAVE_IMAGE("mser.bmp", source);
}*/

#define GENERATEFOLDER


#ifdef TEST_PDF

#include "generation.h"

int main(int argc, char**argv){
    /*struct pdf_info info = {
        .creator = "OCR User",
        .producer = "OCR by D.F. and T.B. and N.K.",
        .title = "OCR output",
        .author = "OCR User",
        .subject = "ocr output",
        .date = "Today"
    };
    struct pdf_doc *pdf = pdf_create(PDF_A4_WIDTH, PDF_A4_HEIGHT, &info);
    pdf_set_font(pdf, "Times-Roman");
    pdf_append_page(pdf);
    pdf_add_text(pdf, NULL, "Hello, world", 12, 30, PDF_A4_HEIGHT-70, PDF_BLACK);
    pdf_save(pdf, "output.pdf");
    pdf_destroy(pdf);*/

    #define str "== = = = = = = = = = = = = = = = = = _ _ _ _ = = _ _ = = = = = = = = = = = = _ _ = _ _ = _ _ = = = = = = = _ = | _ _ _ = = = _ = _ = = ( = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    write_to(str, "testing", html);
    write_to(str, "testing", txt);
    write_to(str, "testing", pdf);
    write_to(str, "testing", odt);
    return 0;
}

#else
#ifdef MINIBATCH

int main(int argc, char** argv) {
    char CMP_BEGIN;
    sscanf(argv[1], "%c", &CMP_BEGIN);
    char MIN_VALUE;
    sscanf(argv[1], "%c", &MIN_VALUE);
    char MAX_VALUE;
    sscanf(argv[1], "%c", &MAX_VALUE);
    const char* path = "/home/daniel/datasets/ocr/script1/complete_1";
    char dir[100];
    for(char tested = CMP_BEGIN; tested < MAX_VALUE; tested++){
        vector_vector_Neuron nn = init_network(DIMENSION*DIMENSION, 15, 2, 1);
        vector_vector_float dataset = vector_of_vector_float(0);
        vector_vector_float expected = vector_of_vector_float(0);
        for(int c = MIN_VALUE; c < MAX_VALUE; c++){
            //push_back_for_vector_float(&expected, init_vector_float(124-MIN_VALUE, 0));
            //nth(nth(expected, expected.element_count-1), c-MIN_VALUE) = 1;
            if(c == tested) push_back_for_vector_float(&expected, tc_cs1(float, 1));
            else push_back_for_vector_float(&expected, tc_cs1(float, 0));
            for(int i = 0; i < IMG_NUMBER; i++){
                sprintf(dir, "%s/%03d/%d.bmp\0", path, c, i);
                BMP_Image* img = BMP_open(dir);
                GreyScale_Image* grey = ColorToGreyScale(img);
                //ApplyTreshHoldbin(grey, 140);
                vector_float layer = image_to_layer(*grey);
                push_back_for_vector_float(&dataset, layer);
                FREEMAGE(img);
                FREEMAGE(grey);
                
            }
        }
        train_network(&nn, dataset, expected, 50000);
        save_nn(nn, tested);
        for(int c = MIN_VALUE; c < MAX_VALUE; c++){
            sprintf(dir, "%s/%03d/0.bmp\0", path, c);
            BMP_Image* img = BMP_open(dir);
            GreyScale_Image* grey = ColorToGreyScale(img);
            //ApplyTreshHoldbin(grey, 140);
            vector_float layer = image_to_layer(*grey);
            vector_float r = forward_propagate(&nn, layer);
            FREEMAGE(grey);
            FREEMAGE(img);
            printf("Character %c identified for '%c' with %f%% accuracy.\n", tested, c, nth(r, 0)*100);
            delete_vec(layer);
            delete_vec(r);
        }
        //free_tensor2(expected);
        free_tensor2(dataset);
        clearNetwork(nn);
    }
    return 0;
}

#else
#ifdef GENERATEFOLDER

int main() {
    BMP_Image* source = BMP_open("data3.bmp");
    // BMP_Image* source = BMP_open("./src/test1.bmp");
    // BMP_Image* source = BMP_open("./src/txt_droit.bmp");
    SAVE_IMAGE("lolilol.bmp", source);
    GreyScale_Image* grey = ColorToGreyScale(source);
    BMP_SAVE("imagesourcedesesmorts.bmp", grey);
    Invert(grey);
    ApplyTreshHold(grey, 100);
    PixelDescriptor** mask;
    vector_RegionDescriptor descriptors;
    vector_MserRegion reg = MserFindCharacter(grey, &mask, &descriptors, HIGH_TRESHOLD(grey->number_of_pixels), 10);

    char buffer[150];
    char* path = "./datasetn2";
    int y = 0;
    for (size_t i = 0; i < reg.element_count; i++, y++) {
        sprintf(buffer, "%s/%03d/n", path, i+33);
        FILE* in = fopen(buffer, "r");
        int fnnum = 1;
        if(in){
            fscanf(in, "%d", &fnnum);
            fclose(in);
        }
        char buff[DIMENSION];
        GreyScale_Image* result = MserToGreyScale(&reg.data[i], mask);
        // GreyScale_Image* result = ExtractRegion(grey, &reg.data[i].location);
        sprintf(buff, "%s/%03d/%i.bmp", path, i+33, fnnum);
        //BMP_SAVE(buff, result);
        FREEMAGE(result);
    }
    drawMser(source, &reg, mask);
    DrawColorVectorMserRegion(source, &reg, 255, 0, 0);
    SAVE_IMAGE("mser.bmp", source);
    //BMP_SAVE("msergrey.bmp", grey);

    delete2D(mask, source->number_of_rows);
    delete_vec(descriptors);
    delete_vec(reg);
    FREEMAGE(source);
    FREEMAGE(grey);
}
/*    for(size_t i = 0; i < reg.element_count-1; i++) {
        if( reg.data[i].location.x < reg.data[i+1].location.x  && 
            reg.data[i+1].location.x < reg.data[i].location.x + reg.data[i].location.width &&
            reg.data[i].location.y < reg.data[i+1].location.y  && 
            reg.data[i+1].location.x < reg.data[i].location.x + reg.data[i].location.width){
                uint32_t old = reg.data[i+1].descriptor.id;
                reg.data[i+1].descriptor.id = reg.data[i].descriptor.id;
                for(size_t j = 0; j < reg.data[i+1].location.width; j++){
                    for(size_t k = 0; k < reg.data[i+1].location.height; k++){
                        if(mask[k+reg.data[i+1].location.y][reg.data[i+1].location.x].id == old){
                            mask[k+reg.data[i+1].location.y][reg.data[i+1].location.x].id = 
                                reg.data[i].descriptor.id;
                        }
                    }
                }

            }
        //mettre à jour id
        //mettre à jour l'identifiant sur le masque
        //la location de la région trop petite est sur le masque (.location du reg.data[i])
    }
*/

#else
#ifdef KNN

#define index 304
int main(){
    vector_Tagged dataset = dataset_to_tagged("/home/daniel/datasets/ocr/script1/", 10, 33, 126);
    vector_Tagged dataset2 = dataset_to_tagged("/home/daniel/datasets/ocr/script1/", 5, 33, 126);
    printf("\nPredicted : %c, expected %c\n", predict(dataset.data[index].p, dataset, 3), dataset.data[index].cat);
}

#else
#ifdef FINISHED

int main(){
    vector_vector_vector_Neuron AI = load_all_nns();
    BMP_Image* img = BMP_open("/home/daniel/datasets/ocr/script1/complete_1/105/0.bmp");
    GreyScale_Image* grey = ColorToGreyScale(img);
    printf("INDENTIFIED : %d %c\n", identify(AI, grey), identify(AI, grey));

}

#else

int main() {
    const char* path = "/home/daniel/datasets/ocr/script1/complete_1";
    char dir[100];
    for(char tested = CMP_BEGIN; tested < MAX_VALUE; tested++){
        vector_vector_Neuron nn = init_network(DIMENSION*DIMENSION, 15, 2, 1);
        vector_vector_float dataset = vector_of_vector_float(0);
        vector_vector_float expected = vector_of_vector_float(0);
        for(int c = MIN_VALUE; c < MAX_VALUE; c++){
            //push_back_for_vector_float(&expected, init_vector_float(124-MIN_VALUE, 0));
            //nth(nth(expected, expected.element_count-1), c-MIN_VALUE) = 1;
            if(c == tested) push_back_for_vector_float(&expected, tc_cs1(float, 1));
            else push_back_for_vector_float(&expected, tc_cs1(float, 0));
            for(int i = 0; i < IMG_NUMBER; i++){
                sprintf(dir, "%s/%03d/%d.bmp\0", path, c, i);
                BMP_Image* img = BMP_open(dir);
                GreyScale_Image* grey = ColorToGreyScale(img);
                //ApplyTreshHoldbin(grey, 140);
                vector_float layer = image_to_layer(*grey);
                push_back_for_vector_float(&dataset, layer);
                FREEMAGE(img);
                FREEMAGE(grey);
                
            }
        }
        train_network(&nn, dataset, expected, 50000);
        save_nn(nn, tested);
        for(int c = MIN_VALUE; c < MAX_VALUE; c++){
            sprintf(dir, "%s/%03d/0.bmp\0", path, c);
            BMP_Image* img = BMP_open(dir);
            GreyScale_Image* grey = ColorToGreyScale(img);
            //ApplyTreshHoldbin(grey, 140);
            vector_float layer = image_to_layer(*grey);
            vector_float r = forward_propagate(&nn, layer);
            FREEMAGE(grey);
            FREEMAGE(img);
            printf("Character %c identified for '%c' with %f%% accuracy.\n", tested, c, nth(r, 0)*100);
            delete_vec(layer);
            delete_vec(r);
        }
        //free_tensor2(expected);
        free_tensor2(dataset);
        clearNetwork(nn);
    }
    return 0;
}

#endif
#endif
#endif
#endif
#endif
            /*size_t maxpos = 0;
            float max_value = 0;
            for(size_t i = 0; i < MAX_VALUE-MIN_VALUE; i++){
                if(nth(r, i) > max_value){
                    max_value = nth(r, i);
                    maxpos = i;
                }
                printf("\t%f\n", nth(r, i));
            }*/
    //vector_float output;
    //output = forward_propagate(&nn, tc_cs1(float, 1, 0));
    /*printf("test for [1 xor 0]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 0, 1));
    printf("test for [0 xor 1]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 1, 1));
    printf("test for [1 xor 1]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 0, 0));
    printf("test for [0 xor 0]: %f\n", nth(output, 0));*/
    //for(size_t i = 0; i < r.element_count; i++)
    //    printf("%f\t", nth(r, i));
    
    //vector_vector_float expected = tc_cs2(float, tc_cs1(float, 1, 0), tc_cs1(float, 0, 1));
    /*for(char c = 33; c < 127; c++){
        push_back_for_vector_float(&expected, vector_of_float(8));
        for(int64_t i = 0; i < 8; i++)
            push_back_for_float(&(nth(expected, c-33)), (float)((c >> i) & 0x01));
    }*/
    /*for(int i = 0; i <= 0; i++ ){
        printf("%d\n", i);
        vector_vector_float batch = tc_cs2(float, tc_cs1(float, 0, 0)) //vector_of_vector_float(128-33);
        for(int c = 97; c < 99; c++){
            //printf("\t%d\n", c);
            sprintf(dir, "%s/%03d/%d.bmp\0", path, c, i);
            BMP_Image* img = BMP_open(dir);
            GreyScale_Image* grey = ColorToGreyScale(img);
            vector_float layer = image_to_layer(*grey);
            push_back_for_vector_float(&batch, layer);
            FREEMAGE(img);
            FREEMAGE(grey);
        }
        train_batch(&nn, batch, expected, 10000);
        delete_vec(batch);
        //free_tensor2(batch);
    }*/
    //save_nn(nn);
    /*size_t result = 0;
    double output[8];
    float seuil;
    for(size_t j = 0;  j < 8; j++){
        result += (output[j] > seuil) << j;
    }*/

    /*vector_vector_float expected = tc_cs2(float, tc_cs1(float, 0), tc_cs1(float, 1));
    vector_vector_float batch = tc_cs2(float, tc_cs1(float, 0, 0), tc_cs1(float, 0, 1));
    train_batch(&nn, batch, expected, 25000);
    vector_vector_float batch2 = tc_cs2(float, tc_cs1(float, 1, 1), tc_cs1(float, 1, 0));
    train_batch(&nn, batch2, expected, 25000);*/