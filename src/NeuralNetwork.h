/**
 * @file NeuralNetwork.h
 * @authors daniel.frederic tanguy.baltazart
 * @brief Declarations for neural network
 * @version 0.1
 * @date 2020-10-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef NEURALNETWORK_HEADER_INCLUDED
#define NEURALNETWORK_HEADER_INCLUDED

#include <math.h>

/**
 * @brief The number of neurone per hidden layer
 * 
 */
#define NEURONE_PER_LAYER 8

typedef double prob;

/**
 * @brief sigmoid transfer function
 * @author daniel.frederic
 * @param x parameter
 * @return double 
 */
double sigmoid(double x) {
     return 1 / (1 + exp(-x)); 
}

/**
 * @brief derivative of sigmoid for backprop
 * @author daniel.frederic
 * @param x 
 * @return double 
 */
double dSigmoid(double x) { 
    return x * (1 - x); 
}

/**
 * @brief neurone
 * @author tanguy.baltazart
 */
typedef struct{
    prob activation;

} neurone;

/**
 * @brief layer
 * @author tanguy.baltazart
 */
typedef struct{
    neurone content[NEURONE_PER_LAYER];
} layer;

#endif