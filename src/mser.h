#ifndef OCR_MSR_H_INCLUDED
#define OCR_MSR_H_INCLUDED
#include "ImageProcessing.h"
#include "queue.h"
#include "tensor.h"
#include <stdbool.h>
#include <stdint.h>

#define LOW_TRESHOLD(size) 10
#define HIGH_TRESHOLD(size) (size / 100)
#define ACCURACY 1

#define delete2D(array, size)                                                  \
    for (size_t i = 0; i < size; i++)                                          \
        free((array[i]));                                                      \
    free(array);


/**
 * @brief Describe a region
 * A region has an id, a size, and a base color
 * @author tanguy.baltazart
 */
typedef struct RegionDescriptor {
    GreyScale_Pixel baseColor;
    uint32_t id;
    size_t size;
} RegionDescriptor;

/**
 * @brief Describe a pixel
 * A pixel has an id and a visited tag.
 * @author tanguy.baltazart
 */
typedef struct PixelDescriptor {
    uint32_t id;
    bool visited;
} PixelDescriptor;

/**
 * @brief Contain a descriptor and the location for a specific region.
 * region.
 * @author tanguy.baltazart
 */
typedef struct MserRegion {
    RegionDescriptor descriptor;
    Region location;
} MserRegion;

QUEUE(uint32_t);
VEC(RegionDescriptor);
VEC(MserRegion);
VEC(vector_MserRegion);

/**
 * @brief Get a 2D array containing descriptor for each pixel with the pixel id
 * and visited set to false.
 *
 * @param image The source image to create the mask from.
 * @return PixelDescriptor**
 * @author tanguy.baltazart daniel.frederic
 */
PixelDescriptor** GetPixelIdMask(GreyScale_Image* image) {

    PixelDescriptor** region = (PixelDescriptor**)malloc(
        image->number_of_rows * sizeof(PixelDescriptor*));
    for (uint32_t i = 0; i < image->number_of_rows; i++) {
        region[i] = (PixelDescriptor*)malloc(image->pixels_per_row *
                                             sizeof(PixelDescriptor));
        for (uint32_t j = 0; j < image->pixels_per_row; j++) {
            PixelDescriptor reg = {.id = (i * image->pixels_per_row) + j,
                                   .visited = false};
            region[i][j] = reg;
        }
    }

    return region;
}

/**
 * @brief Binary search for a specific id in an MSER Region.
 *
 * @param regions_descriptors The vector of MSER region to search in
 * @param id the id of the region.
 * @return the position of the region if found otherwise the lenght of the
 * vector.
 * @author tanguy.baltazart
 */
size_t bin_search2(vector_MserRegion* regions_descriptors, uint32_t id) {
    if (regions_descriptors->element_count == 0)
        return 0;

    size_t low = 0;
    size_t hight = regions_descriptors->element_count - 1;
    size_t middle = (low + hight) / 2;

    while (middle < regions_descriptors->element_count && low <= hight &&
           regions_descriptors->data[middle].descriptor.id != id) {

        if (regions_descriptors->data[middle].descriptor.id > id) {
            hight = middle - 1;
        }
        if (regions_descriptors->data[middle].descriptor.id < id) {
            low = middle + 1;
        }
        middle = (low + hight) / 2;
    }
    if (middle > regions_descriptors->element_count || low > hight)
        return regions_descriptors->element_count;

    return middle;
}
/**
 * @brief Binary search for a specific id in the regions_descriptors.
 *
 * @param regions_descriptors The region to search in
 * @param id the id of the region.
 * @return the position of the region if found other wise the lenght of the
 * vector.
 * @author tanguy.baltazart
 */
size_t bin_search(vector_RegionDescriptor* regions_descriptors, uint32_t id) {
    if (regions_descriptors->element_count == 0)
        return 0;

    size_t low = 0;
    size_t hight = regions_descriptors->element_count - 1;
    size_t middle = (low + hight) / 2;

    while (middle < regions_descriptors->element_count && low <= hight &&
           regions_descriptors->data[middle].id != id) {

        if (regions_descriptors->data[middle].id > id) {
            hight = middle - 1;
        }
        if (regions_descriptors->data[middle].id < id) {
            low = middle + 1;
        }
        middle = (low + hight) / 2;
    }
    if (middle > regions_descriptors->element_count || low > hight)
        return regions_descriptors->element_count;

    return middle;
}

/**
 * @brief Sequential search for the value in the vector MserRegion
 * 
 * @param regions The vector of MserRegion to search in
 * @param id The value to search
 * @return the position of the element if found otherwise the lenght of the array.
 */
size_t seq_search(vector_MserRegion* regions, uint32_t id){

    size_t i = 0;
    while(i < regions->element_count && regions->data[i].descriptor.id != id)
        i+=1;

    return i;
}


bool CheckMask(PixelDescriptor** mask, int32_t y, int32_t x,
               queue_uint32_t* upcomming, int32_t size, uint32_t currid) {
    if (!mask[y][x].visited)
        return true;

    if (mask[y][x].id != currid)
        enqueue_uint32_t(upcomming, y * size + x);
    return false;
}

/**
 * @brief Return the information for one specific region.
 *
 * @param image The image from wich the region came.
 * @param mask  The mask containing the id of all different pixel.
 * @param location The location of the first pixel of the region.
 * @param accuracy The light interval of wich a pixel is considered in the
 * region.
 * @param tovisit The queue representing the pixel not in the region to visit.
 * @return RegionDescriptor
 */
RegionDescriptor GetRegion(GreyScale_Image* image, PixelDescriptor** mask,
                           uint32_t location, uint32_t id, int16_t accuracy,
                           queue_uint32_t* tovisit) {
#define getIndex(i, j) (i) * image->pixels_per_row + (j)

    uint32_t x = location % image->pixels_per_row;
    uint32_t y = location / image->pixels_per_row;
    RegionDescriptor reg = {
        .baseColor = image->data[y][x], .id = id, .size = 0};

    mask[y][x].visited = true;

    // #define CheckPos(y, x)
    //     (!mask[y][x].visited) && image->data[y][x] == reg.baseColor

#define CheckPos(y, x)                                                         \
    (image->data[y][x] < reg.baseColor + accuracy) &&                          \
        (image->data[y][x] > reg.baseColor - accuracy)
    //  CheckMask(mask, y, x, upcomming, image->pixels_per_row, reg.id)

    queue_uint32_t* queue = QUEUE_uint32_t();
    enqueue_uint32_t(queue, location);

    while ((!IsEmpty(queue))) {
        int rep;
        int32_t currentNode = dequeue_uint32_t(queue, &rep);
        x = currentNode % image->pixels_per_row;
        y = currentNode / image->pixels_per_row;

        mask[y][x].id = reg.id;
        reg.size++;

        for (char i = -1; i <= 1; i++) {
            for (char j = -1; j <= 1; j++) {
                if ((i != 0 || j != 0) && (int32_t)(y + i) >= 0 &&
                    (y + i) < image->number_of_rows && (int32_t)(x + j) >= 0 &&
                    (x + j) < image->pixels_per_row) {

                    if (!mask[y + i][x + j].visited) {
                        if (CheckPos(y + i, x + j)) {
                            enqueue_uint32_t(queue, getIndex(y + i, x + j));
                            mask[y + i][x + j].visited = true;
                        } else {
                            enqueue_uint32_t(tovisit, getIndex(y + i, x + j));
                        }
                    }
                }
            }
        }
    }
    free(queue);
    return reg;
}

/**
 * @brief Locate all different pixel region inside the image.
 *
 * @param image The image to search in.
 * @param mask The descriptor of
 * @param accuracy The mask containing the id of all different pixel.
 * @return A list containing all region found and their size.
 * @author tanguy.baltazart
 */
vector_RegionDescriptor GetRegionList(GreyScale_Image* image,
                                      PixelDescriptor** mask, int16_t accuracy,
                                      uint32_t low, uint32_t high) {
    vector_RegionDescriptor regionList = vector_of_RegionDescriptor(0);

#define getIndex(i, j) (i) * image->pixels_per_row + (j)

    queue_uint32_t* queue = QUEUE_uint32_t();
    enqueue_uint32_t(queue, 0);
    int succ;
    uint32_t id = 0;
    while (!IsEmpty(queue)) {
        uint32_t pos = dequeue_uint32_t(queue, &succ);

        uint32_t x = pos % image->pixels_per_row;
        uint32_t y = pos / image->pixels_per_row;
        if (!mask[y][x].visited) {
            RegionDescriptor reg =
                GetRegion(image, mask, pos, id, accuracy, queue);
            if (reg.size > low && reg.size < high)
                push_back_for_RegionDescriptor(&regionList, reg);
            id++;
        }
    }

    free(queue);
    return regionList;
}

size_t countElement(vector_RegionDescriptor* list, uint32_t element,
                    size_t start) {
    size_t sum = 0;
    size_t i = start;
    // bool found = false;
    for (; i < list->element_count; i++) {
        sum += list->data[i].id == element;
    }
    // if (!found)
    // *start = list->element_count;
    return sum;
}

/**
 * @brief Draw the mser region on the source image.
 *
 * @param source The image to draw on
 * @param regionList The vector of MSER region.
 * @param mask The mask containing all pixel id.
 * @author tanguy.baltazart
 */
void drawMser(BMP_Image* source, vector_MserRegion* regions,
              PixelDescriptor** mask) {
    int colors[] = {0xf94144, 0xf3722c, 0xf9844a, 0xf9c74f, 0x90be6d,
                    0x43aa8b, 0x4d908e, 0x577590, 0x277da1};
    //int colors2[] = {0xd8e2dc, 0xffe5d9, 0xffcad4, 0xf4acb7,
     //                0x9d8189, 0xedddd4, 0xc8e7ff};

    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        for (uint32_t j = 0; j < source->pixels_per_row; j++) {

            size_t pos = seq_search(regions, mask[i][j].id);
            int rgb = 0x515151;

            if (pos < regions->element_count) {
                rgb = colors[mask[i][j].id % 9];
            }

            source->data[i][j].R = rgb >> 16;
            source->data[i][j].G = (rgb >> 8) & 0xFF;
            source->data[i][j].B = rgb & 0xFF;
        }
    }
}


/**
 * @brief Draw the mser region on the source image using a vector Region descriptor.
 * 
 * @param source The image to draw on
 * @param regions The vector of region descriptor
 * @param mask The mask containing descriptor for each pixel.
 * @author tanguy.baltazart
 */
void drawMser2(BMP_Image* source, vector_RegionDescriptor* regions,
               PixelDescriptor** mask) {
    int colors[] = {0xf94144, 0xf3722c, 0xf9844a, 0xf9c74f, 0x90be6d,
                    0x43aa8b, 0x4d908e, 0x577590, 0x277da1};
    //int colors2[] = {0xd8e2dc, 0xffe5d9, 0xffcad4, 0xf4acb7,
    //                0x9d8189, 0xedddd4, 0xc8e7ff};

    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        for (uint32_t j = 0; j < source->pixels_per_row; j++) {

            size_t pos = bin_search(regions, mask[i][j].id);

            // int rgb = colors2[mask[i][j].id % 7];
            int rgb = 0x515151;

            if (pos < regions->element_count) {
                rgb = colors[mask[i][j].id % 9];
            }

            source->data[i][j].R = rgb >> 16;
            source->data[i][j].G = (rgb >> 8) & 0xFF;
            source->data[i][j].B = rgb & 0xFF;
        }
    }
}

/**
 * @brief Replace a color by another in the region's list
 *
 * @param regionList The region in wich the color need to be replace.
 * @param color The color to replace.
 * @param newColor The new color.
 * @author tanguy.baltazart
 */
void AnnihilateColor(vector_RegionDescriptor* regionList, GreyScale_Pixel color,
                     GreyScale_Pixel newColor) {
    for (size_t i = 0; i < regionList->element_count; i++) {
        if (regionList->data[i].baseColor == color)
            regionList->data[i].baseColor = newColor;
    }
}

void FilterColor(vector_RegionDescriptor* regionList) {
    for (size_t i = 0; i < regionList->element_count; i++) {
        if (regionList->data[i].size > HIGH_TRESHOLD(regionList->element_count))
            AnnihilateColor(regionList, regionList->data[i].baseColor, 0);
    }
}

void MaskImage(GreyScale_Image* source, vector_RegionDescriptor* regionList,
               PixelDescriptor** mask) {

    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        for (uint32_t j = 0; j < source->pixels_per_row; j++) {

            RegionDescriptor region =
                regionList->data[bin_search(regionList, mask[i][j].id)];
            // int rgb = (region.baseColor * 0x00010101) + region.id;
            if (region.size > HIGH_TRESHOLD(regionList->element_count))
                source->data[i][j] = 255;
        }
    }
}

/**
 * @brief Draw the mser region on the source image.
 *
 * @param source The image to draw in.
 * @param regionList The list of all region.
 * @param mask  The mask containing all pixel id.
 * @author tanguy.baltazart
 */
void DrawMserGrey(GreyScale_Image* source, vector_RegionDescriptor* regionList,
                  PixelDescriptor** mask) {

    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        for (uint32_t j = 0; j < source->pixels_per_row; j++) {

            RegionDescriptor region =
                regionList->data[bin_search(regionList, mask[i][j].id)];
            // int rgb = (region.baseColor * 0x00010101) + region.id;
            source->data[i][j] = region.baseColor;
        }
    }
}
/**
 * @brief Return the box in which the region is included.
 *
 * @param source The image it's for knowing the size.
 * @param region The region to found in the mask.
 * @param mask The mask containing all pixel id.
 * @return A box containing the region.
 * @author tanguy.baltazart
 */
Region ExtractCharRegion(GreyScale_Image* source, RegionDescriptor* region,
                         PixelDescriptor** mask) {

    Region location = {.x = source->pixels_per_row,
                       .y = source->number_of_rows,
                       .width = 1,
                       .height = 1};

    for (uint32_t i = 0; i < source->number_of_rows; i++) {
        for (uint32_t j = 0; j < source->pixels_per_row; j++) {

            if (mask[i][j].id == region->id) {
                if (i < location.y) {
                    location.y = i;
                    location.height++;
                } else if (location.y + location.height < i) {
                    location.height++;
                }

                if (j < location.x) {
                    location.x = j;
                    location.width++;
                } else if (location.x + location.width < j) {
                    location.width++;
                }
            }
        }
    }

    return location;
}

/**
 * @brief Check if a region contain a character.
 *
 * @param image The image from which the region came.
 * @param region The box that should contain a character.
 * @return true If the box contain a character
 * @return false If the box doesn't contain a character.
 */
bool IsCharRegion(Region* region) {
    return region->height > 0 && region->width > 0;
}

/**
 * @brief List all box containing region in the image.
 *
 * @param source The image to extract box from.
 * @param region The list of regions inside the image.
 * @param mask The mask containing all pixel id.
 * @return vector_Region The list containing all region
 * @author tanguy.baltazart
 */
vector_MserRegion GetMserRegions(GreyScale_Image* source,
                                 vector_RegionDescriptor* regions,
                                 PixelDescriptor** mask) {
    vector_MserRegion foundRegion = vector_of_MserRegion(0);

    for (size_t i = 0; i < regions->element_count; i++) {
        if (regions->data[i].size <
                (size_t)HIGH_TRESHOLD(source->number_of_pixels) &&
            regions->data[i].size >
                (size_t)LOW_TRESHOLD(source->number_of_pixels)) {
            Region reg = ExtractCharRegion(source, &regions->data[i], mask);
            MserRegion newReg = {.descriptor = regions->data[i],
                                 .location = reg};
            if (IsCharRegion(&reg))
                push_back_for_MserRegion(&foundRegion, newReg);
        }
    }
    return foundRegion;
}


/**
 * @brief List all MSER Region in an image and return them in their order of apparition.
 * 
 * @param source The image to search region in.
 * @param regions This parameter will be modifed. The list of Region Descriptor found.
 * @param mask The mask containing descriptor for each pixel
 * @return vector_MserRegion 
 */
vector_MserRegion GetMserRegions2(GreyScale_Image* source,
                                  vector_RegionDescriptor* regions,
                                  PixelDescriptor** mask) {
    vector_MserRegion foundRegion = vector_of_MserRegion(0);

    for (int32_t i = source->number_of_rows - 11; i >= 0; i -= 20) {
        for (size_t j = 0; j < source->pixels_per_row; j++) {

            for (int32_t z = i + 10; z > 0 && z > i - 10; z--) {

                if (mask[z][j].visited) {
                    mask[z][j].visited = false;
                    size_t pos = bin_search(regions, mask[z][j].id);

                    if (pos == regions->element_count)
                        continue;

                    RegionDescriptor currentReg = regions->data[pos];

                    if (currentReg.size <
                            (size_t)HIGH_TRESHOLD(source->number_of_pixels) &&
                        currentReg.size >
                            (size_t)LOW_TRESHOLD(source->number_of_pixels)) {
                        Region reg =
                            ExtractCharRegion(source, &currentReg, mask);
                        MserRegion newReg = {
                            .descriptor =
                                (RegionDescriptor){currentReg.baseColor,
                                                   currentReg.id,
                                                   currentReg.size},
                            .location = reg};
                        if (IsCharRegion(&reg))
                            push_back_for_MserRegion(&foundRegion, newReg);
                        Remove_at_for_RegionDescriptor(regions, pos);
                    }
                }
            }
        }
    }

    return foundRegion;
}


/**
 * @brief Remove regions included in other region.
 *
 * @param regions The regions in which the nested regions must be removed.
 * @author tanguy.baltazart
 */
void RemoveIncludedMserRegions(vector_MserRegion* regions) {

    for (size_t i = 0; i < regions->element_count; i++) {
        for (size_t j = i + 1; j < regions->element_count; j++) {
            if (IsRegionIncluded(regions->data[i].location,
                                 regions->data[j].location)) {
                Remove_at_for_MserRegion(regions, j);
                j--;
            }
        }
    }
}

/**
 * @brief Return the list of MserRegion which should contain a character using
 * the MSER algorithm.
 *
 * @param source The image in which the Region must be searched.
 * @param mask The mask containing all pixel id
 * @param accuracy The accuracy is the difference value of which two pixel will
 * be considered in the same region.
 * @return vector_MserRegion The list containing the location and the descritors
 * of the region.
 * @author tanguy.baltazart
 */
vector_MserRegion MserFindCharacter(GreyScale_Image* source,
                                    PixelDescriptor*** mask,
                                    vector_RegionDescriptor* descriptors,
                                    uint32_t hight, uint32_t low) {
    *mask = GetPixelIdMask(source);
    *descriptors = GetRegionList(source, *mask,20 , low,
                                 source->number_of_pixels / hight);
    vector_MserRegion regions = GetMserRegions2(source, descriptors, *mask);
    RemoveIncludedMserRegions(&regions);
    return regions;
}

/**
 * @brief Draw in the the image for each location in the MserRegion.
 *
 * @param source The image to draw on
 * @param regions The list of Mserregion to draw.
 * @param r The red component of the pixel.
 * @param g The green component of the pixel.
 * @param b The blue component of the pixel.
 * @author tanguy.baltazart
 */
void DrawColorVectorMserRegion(BMP_Image* source, vector_MserRegion* regions,
                               uint8_t r, uint8_t g, uint8_t b) {
    for (size_t i = 0; i < regions->element_count; i++) {
        DrawColorRegion(source, &(regions->data[i].location), r, g, b);
    }
}


#define MAX(a, b) (a > b) ? a : b
/**
 * @brief Convert an MSER region into a GreyScale image the background of the image will be black and the foreground white.
 * 
 * @param region The region containing the id of the foreground pixel.
 * @param mask The mask of descrpitor for each pixel.
 * @return GreyScale_Image* 
 */
GreyScale_Image* MserToGreyScale(MserRegion* region, PixelDescriptor** mask) {
    uint32_t width = region->location.width+1;
    uint32_t height = region->location.height+1;
    size_t size = MAX(width, height) + 10;
    GreyScale_Image* image = Create_Image(size, size, 0);

    size_t offset_i = (size - width) / 2;
    size_t offset_j = (size - height) / 2;

    Region reg = region->location;

    for (size_t j = offset_j; j < size; j++) {
        for (size_t i = offset_i; i < size; i++) {
            size_t x = i - offset_i;
            size_t y = j - offset_j;
            image->data[j][i] =
                (mask[reg.y + y][reg.x + x].id == region->descriptor.id) * 255;

        }
    }

    return image;
}
#undef MAX
#endif