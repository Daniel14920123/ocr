/**
 * @file myneural.h
 * @author tanguy.baltazart daniel.frederic
 * @brief restarting from scratch after failure of implementation of daniel.frederic
 * @version 0.1
 * @date 2020-10-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef MY_NEURAL
#define MY_NEURAL

#include "math.h"
#include "stdio.h"
#include "tensor_def.h"

/**
 * @brief Macro for probability (normal distribution between 0 and 1)
 * 
 */
#define PROBA ((double)rand() / (double)RAND_MAX)

/**
 * @brief The transfer function, sigmoid here
 * 
 * @param activation The result of the activation function applied on previous layer
 * @return float The computed input
 */
float transfer(float activation) { return 1.0 / (1.0 + expf(-activation)); }

/**
 * @brief derivative of sigmoid
 * @author tanguy.baltazart
 * @param x input
 * @return float compute input
 */
float dsigmoid(float x) { return x * (1 - x); }

/**
 * @brief Random value between -1 and 1 with pseudo normal distribution
 * @author daniel.frederic
 * @return double 
 */
double init_random_prob() {
    return ((double)rand() / (double)RAND_MAX) * 2 - 1;
}

/**
 * @brief Structure for a neuron
 * @author tanguy.baltazart
 */
typedef struct {
    vector_float weights;
    float delta;
    float output;
    vector_float deltas_batch;
} Neuron;

TENSOR_init2(Neuron);

/**
 * @brief Initialize the neural networks (tensor declaration, number of layers...)
 * @author tanguy.baltazart
 * @param n_inputs length of input layer
 * @param n_hidden number of hidden layer
 * @param n_outputs length of ouput
 * @return vector_vector_Neuron 
 */
vector_vector_Neuron init_network(size_t n_inputs, size_t n_hidden, size_t hidden_count,
                                  size_t n_outputs) {
    vector_vector_Neuron network = vector_of_vector_Neuron(0);
    for(size_t z = 0; z < hidden_count; z++){
        vector_Neuron hidden_layer = vector_of_Neuron(0);
        for (size_t j = 0; j < n_hidden; j++) {
            Neuron myneuron;
            myneuron.weights = vector_of_float(0);
            myneuron.delta = 0;
            myneuron.output = 0;
            myneuron.deltas_batch = vector_of_float(100);

            for (size_t i = 0; i < n_inputs + 1; i++) {
                push_back_for_float(&myneuron.weights, init_random_prob());
            }
            push_back_for_Neuron(&hidden_layer, myneuron);
        }

        push_back_for_vector_Neuron(&network, hidden_layer);
    }
    vector_Neuron output_layer = vector_of_Neuron(0);

    for (size_t j = 0; j < n_outputs; j++) {
        Neuron myneuron;
        myneuron.weights = vector_of_float(0);
        myneuron.delta = 0;
        myneuron.output = 0;
        myneuron.deltas_batch = vector_of_float(100);

        for (size_t i = 0; i < n_hidden + 1; i++) {
            push_back_for_float(&myneuron.weights, init_random_prob());
        }
        push_back_for_Neuron(&output_layer, myneuron);
    }
    push_back_for_vector_Neuron(&network, output_layer);

    return network;
}

/**
 * @brief Ponderated sum of previous layer (with weights)
 * @author tanguy.baltazart
 * @param weights The weights
 * @param inputs The datas stored
 * @return float The result of activation function
 */
float activate(vector_float weights, vector_float inputs) {
    float activation = nth(weights, weights.element_count - 1);
    for (size_t i = 0; i < weights.element_count - 1; i++) {
        activation += nth(weights, i) * nth(inputs, i);
    }

    return activation;
}

/**
 * @brief Forward propagation
 * @authors tanguy.baltazart daniel.frederic
 * @param network Pointer to the neural network
 * @param row The current row, input data at the beginning
 * @return vector_float The result layer
 */
vector_float forward_propagate(vector_vector_Neuron* network,
                               vector_float row) {
    vector_float newInputs = vector_of_float(0);
    vector_float inputs = vector_of_float(0);
    for (size_t j = 0; j < row.element_count; j++) {
        push_back_for_float(&inputs, nth(row, j));
    }
    for (size_t i = 0; i < network->element_count; i++) {

        vector_Neuron* layer = &network->data[i];

        for (size_t j = 0; j < layer->element_count; j++) {
            Neuron* neuron = &layer->data[j];
            float activation = activate(neuron->weights, inputs);
            neuron->output = transfer(activation);
            push_back_for_float(&newInputs, neuron->output);
        }

        inputs.element_count = 0;
        for (size_t j = 0; j < newInputs.element_count; j++) {
            push_back_for_float(&inputs, nth(newInputs, j));
        }
        newInputs.element_count = 0;
    }
    delete_vec(newInputs);
    return inputs;
}

/**
 * @brief Backward propagation
 * @author tanguy.baltazart
 * @param network The neural network
 * @param expected The expected value
 */
void backward_propagate(vector_vector_Neuron* network, vector_float expected) {

    for (int64_t C = network->element_count - 1; C != -1; C--) {
        size_t i = (size_t)C;
        vector_float errors = vector_of_float(0);
        vector_Neuron* layer = &network->data[i];

        if (i != network->element_count - 1) {

            for (size_t j = 0; j < layer->element_count; j++) {
                float error = 0.0;

                for (size_t z = 0; z < network->data[i + 1].element_count;
                     z++) {

                    error += network->data[i + 1].data[z].weights.data[j] *
                             network->data[i + 1].data[z].delta;
                }

                push_back_for_float(&errors, error);
            }

        } else {
            for (size_t j = 0; j < layer->element_count; j++) {
                Neuron neuron = layer->data[j];
                push_back_for_float(&errors, nth(expected, j) - neuron.output);
            }
        }

        for (size_t j = 0; j < layer->element_count; j++) {
            layer->data[j].delta =
                nth(errors, j) * dsigmoid(layer->data[j].output);
        }
        delete_vec(errors);
    }
}

/**
 * @brief Updates the weights in the neural network, using the bias and error gradient
 * @author tanguy.baltazart daniel.frederic
 * @param network Pointer to neural network
 * @param row Current row
 * @param l_rate Learning rate
 */
void update_weights(vector_vector_Neuron* network, vector_float row,
                    float l_rate) {
    vector_float inputs = vector_of_float(0);
    vector_Neuron* layer;

    for (size_t j = 0; j < row.element_count; j++) {
        push_back_for_float(&inputs, nth(row, j));
    }

    for (size_t i = 0; i < network->element_count; i++) {

        if (i != 0) {
            layer = &network->data[i - 1];
            inputs.element_count = 0;
            for (size_t j = 0; j < layer->element_count; j++) {
                push_back_for_float(&inputs, layer->data[j].output);
            }
        }

        layer = &network->data[i];
        for (size_t j = 0; j < layer->element_count; j++) {
            for (size_t z = 0; z < inputs.element_count; z++) {
                layer->data[j].weights.data[z] +=
                    l_rate * layer->data[j].delta * nth(inputs, z);
            }
        }
    }
    delete_vec(inputs);
}

/**
 * @brief Trains a neural network
 * 
 * @param nn Pointer to neural network
 * @param data Data for training
 * @param expected Expected values for last layers
 * @param n_epoch epoch
 */
void train_network(vector_vector_Neuron* nn, vector_vector_float data,
                   vector_vector_float expected, size_t n_epoch) {
    for (size_t j = 0; j < n_epoch; j++) {


        for (size_t i = 0; i < data.element_count; i++) {
            vector_float outputs = forward_propagate(nn, nth(data, i));
            backward_propagate(nn, nth(expected, i));
            update_weights(nn, nth(data, i), 0.5);
            delete_vec(outputs);

            // printf("[ %f, %f] \n", nth(outputs, 0), nth(outputs, 1));
            /* code */
        }
    }
}

/**
 * @brief Free data in neural network
 * @author tanguy.baltazart
 * @param nn The neural network to free
 */
void clearNetwork(vector_vector_Neuron nn){
    for (size_t i = 0; i < nn.element_count; i++)
    {
        vector_Neuron layer = nn.data[i];
        for (size_t j = 0; j < layer.element_count; j++)
        {
            Neuron neuron = layer.data[j];
            delete_vec(neuron.weights);
            delete_vec(neuron.deltas_batch);
        }
        delete_vec(layer);
    }
    delete_vec(nn);
}
#endif