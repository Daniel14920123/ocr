#ifndef QUEUE_GENERIC
#define QUEUE_GENERIC
#include <stdlib.h>

#define QUEUE(T)    \
typedef struct queue_node_##T {     \
    T key;      \
    struct queue_node_##T* next;\
} queue_node_##T;\
\
typedef struct { \
    queue_node_##T * first, *last; \
} queue_##T; \
queue_node_##T * NODE_##T (T to_add) { \
    queue_node_##T * temp = (queue_node_##T *)malloc(sizeof(queue_node_##T)); \
    temp->key = to_add; \
    temp->next = NULL; \
    return temp; \
} \
queue_##T * QUEUE_##T (){ \
    queue_##T * q = (queue_##T *)malloc(sizeof(queue_##T)); \
    q->first = q->last = NULL; \
    return q; \
} \
void enqueue_##T (queue_##T * q, T k) { \
    queue_node_##T * temp = NODE_##T(k); \
    if (q->last == NULL) {   \
        q->first = q->last = temp; \
        return; \
    } \
    q->last->next = temp; \
    q->last = temp; \
} \
T dequeue_##T(queue_##T* q, int* success) { \
    if (q->first == NULL) \
        {*success = 0; return (T)0;} \
    queue_node_##T * temp = q->first; \
    T k = temp->key; \
    q->first = q->first->next; \
    if (q->first == NULL) \
        q->last = NULL; \
    free(temp); \
    *success = 1;\
    return k; \
} 

#define IsEmpty(q) (q->first == NULL)

#endif