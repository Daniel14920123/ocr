CC=gcc

CFLAGS = `pkg-config --cflags gtk+-3.0`-std=c99 -lm -Wall -g -Wextra -I/usr/lib/x86_64-linux-gnu/glib-2.0/include/ -lSDL #-fsanitize=address
LDLIBS = `pkg-config --libs gtk+-3.0`


SRC=./src/main.c
TRAINSRC=./src/train.c
BINNAME=ocr.elf
TRAINNAME=train.elf
DEBUGNAME=ocr.dbg
TRAINDEBUG=train.dbg

all:
	$(CC) $(SRC) -o $(BINNAME) $(CFLAGS) -O3 $(LDLIBS)

train:
	$(CC) $(TRAINSRC) -o $(TRAINNAME) $(CFLAGS) -O3
	./$(TRAINNAME)

run:
	$(CC) $(SRC) -o $(BINNAME) $(CFLAGS)
	./$(BINNAME)

clean:
	rm ./*.elf ./*.dbg

debug:
	$(CC) -g $(SRC) -o $(DEBUGNAME) $(CFLAGS) $(LDLIBS)

debug_train:
	$(CC) -g $(TRAINSRC) -o $(TRAINDEBUG) $(CFLAGS)

valgrind:
	valgrind --leak-check=full --tool=memcheck ./$(DEBUGNAME)

valgrind_train:
	valgrind --leak-check=full --tool=memcheck ./$(TRAINDEBUG)