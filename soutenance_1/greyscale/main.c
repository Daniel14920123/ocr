#include "../../src/ImageProcessing.h"
#include "../../src/ImageLoader.h"
#include <err.h>

int main(int argc, char *argv[])
{

    if(argc < 1){
        errx(1, "An image path must be providden. Aborting.\n");
    }

    BMP_Image* input1 = BMP_open(argv[1]);
    GreyScale_Image * input2 = ColorToGreyScale(input1);
    BMP_SAVE("grayscale.bmp\0", input2);
    free(input1);
    free(input2);
    return 0;
}

