#include "../../src/ImageLoader.h"
#include "../../src/ImageProcessing.h"
#include <err.h>

int main(int argc, char* argv[]) 
{
    //BMP_Image *result = BMP_open("./src/myfile.bmp\0");
    //BMP_Image *result = BMP_open("./src/text (1).bmp\0");
    if (argc > 2)
        errx(1, "Too many arguments. Aborting.");
    BMP_Image* result = BMP_open(argv[1]);
    GreyScale_Image* soluce = ColorToGreyScale(result);
    Invert(soluce); // Pour que le texte soit en blanc.
    ApplyTreshHold(soluce, 100); // Optionelle
    vector_Region regions = GetProjectionProfile(soluce);
    for (size_t j = 0; j < regions.element_count; j++) {
        if (nth(regions, j).height == 0) {
                continue;
            }
    vector_Region column = Expand(soluce,GetColumn(soluce, &nth(regions, j)));
    for (size_t i = 0; i < column.element_count; i++) 
    {
       DrawRegion(soluce, &nth(column, i));
    }
    delete_vec(column);
    }
    BMP_SAVE("sortie.bmp\0", soluce);
    free(soluce);
    return 0;
}
