#include <math.h>
#include "../../src/ImageProcessing.h"
#include "../../src/ImageLoader.h"

/*
input1 = image originale
input2 = image en noir et blanc
input3 = lettre entouree
angle = l'angle
input5 = l'image droite
*/
int main()
{
    BMP_Image* input1 = BMP_open("../image/45.bmp\0");
    GreyScale_Image* input2 = ColorToGreyScale(input1);

    GreyScale_Image* input3 = EdgeDetect(input2);



    double angle = GetAngle(input2)+90.;
    printf("Angle : %f°\n",(angle));
   
    GreyScale_Image* input5 = RotatebyAngle(input2, angle);
    Invert(input5);
    BMP_SAVE("result.bmp\0",input5);
    free(input1);
    free(input2);
    free(input3);
    free(input5);

    return 0;

}

