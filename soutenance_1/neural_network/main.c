#include <stdio.h>
#include "../../src/myneural.h"

int main() {

    

    vector_vector_float dataset = vector_of_vector_float(0);
    push_back_for_vector_float(&dataset, tc_cs1(float, 1, 0));
    push_back_for_vector_float(&dataset, tc_cs1(float, 0, 1));
    push_back_for_vector_float(&dataset, tc_cs1(float, 1, 1));
    push_back_for_vector_float(&dataset, tc_cs1(float, 0, 0));
    // tensor2(float) dataset =
    //     tc_cs2(vector_float, tc_cs1(float, 1, 0), tc_cs1(float, 1, 1),
    //            tc_cs1(float, 0, 1), tc_cs1(float, 0, 0));

    vector_vector_float expected = vector_of_vector_float(0);
    push_back_for_vector_float(&expected, tc_cs1(float, 1));
    push_back_for_vector_float(&expected, tc_cs1(float, 1));
    push_back_for_vector_float(&expected, tc_cs1(float, 0));
    push_back_for_vector_float(&expected, tc_cs1(float, 0));

    // tensor2(float) expected =
    //     tc_cs2(vector_float, tc_cs1(float, 0, 1), tc_cs1(float, 1, 0),
    //             tc_cs1(float, 0, 1), tc_cs1(float, 1, 0));
    // tc_cs3(tensor2(float), tc_cs2(tensor1(float, 0, 1), tensor1(float, )))
    vector_vector_Neuron nn = init_network(2, 5, 1, 1);
    printf("Training dataset\n");
    train_network(&nn, dataset, expected, 25000);


    vector_float output;
    output = forward_propagate(&nn, tc_cs1(float, 1, 0));
    printf("test for [1 xor 0]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 0, 1));
    printf("test for [0 xor 1]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 1, 1));
    printf("test for [1 xor 1]: %f\n", nth(output, 0));
    output = forward_propagate(&nn, tc_cs1(float, 0, 0));
    printf("test for [0 xor 0]: %f\n", nth(output, 0));
    
    clearNetwork(nn);
    return 0;
}
